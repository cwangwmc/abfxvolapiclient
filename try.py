from typing import *
from datetime import datetime
from dateutil.tz import tzlocal, gettz
from kafka import KafkaConsumer, TopicPartition
from pytz import timezone
from kafkaConsumers import SpotSource, VolSource, KafkaBootstrapServer,\
    SpotKafkaConsumer, VolKafkaConsumer, RfqVolKafkaConsumer, TenorDatesKafkaConsumer

import arrow
import volatilitySurface as vs
import tenorDates as td
import xml.etree.ElementTree as et
import logging
import time
import json
import sys


_logger = logging.getLogger()
_logger.setLevel(logging.INFO)

_formatter = logging.Formatter('|%(asctime)s| [%(levelname)s] (%(threadName)-10s) %(message)s')

_console_handler = logging.StreamHandler()
_console_handler.setLevel(logging.DEBUG)
_console_handler.setFormatter(_formatter)

_logger.addHandler(_console_handler)


def try_reading_ccypairs_config():
    currencypairs_config_file = open('currencypairs.config', 'r')
    currencypairs_config_lines = currencypairs_config_file.readlines()
    currencypairs_config_file.close()
    # Getting rid of the trailing line breaks
    currencypairs_config_no_line_breaks = [l[:-1] for l in currencypairs_config_lines]
    for l in currencypairs_config_no_line_breaks:
        _logger.info('[{0}]'.format(l))


def try_timezone():
    datetime_fmt = '%Y-%m-%dT%X%Z'
    # local time zone
    print(datetime.now(tzlocal()).strftime(datetime_fmt))
    # UTC
    print(datetime.utcnow().strftime(datetime_fmt))
    print(datetime.now(timezone('UTC')).strftime(datetime_fmt))
    # US Eastern
    print(datetime.now(timezone('US/Eastern')).strftime(datetime_fmt))
    # Europe London
    print(datetime.now(timezone('Europe/London')).strftime(datetime_fmt))


def try_arrow():
    # local time zone
    local_dt_now = datetime.now().replace(microsecond=0)
    local_arw_now = arrow.get(local_dt_now, tzlocal())
    local_arw_now_iso_fmt = local_arw_now.isoformat()
    local_arw_now_iso_fmt_parsed = arrow.get(local_arw_now_iso_fmt)
    print(local_arw_now_iso_fmt_parsed.isoformat())

    # utc
    utc_dt_now = datetime.utcnow().replace(microsecond=0)
    utc_arw_now = arrow.get(utc_dt_now)
    print(utc_arw_now.isoformat())

    # US Eastern
    est_dt_now = datetime.now(timezone('US/Eastern')).replace(microsecond=0)
    est_arw_now = arrow.get(est_dt_now)
    print(est_arw_now.isoformat())

    # European London
    ldn_dt_now = datetime.now(timezone('Europe/London')).replace(microsecond=0)
    ldn_arw_now = arrow.get(ldn_dt_now)
    print(ldn_arw_now.isoformat())


def try_vols_KafkaConsumer(kbs: KafkaBootstrapServer, ccy_pair_list: List[str], vol_src: VolSource):
    vol_consumer = VolKafkaConsumer(kbs, ccy_pair_list, vol_src)
    ccypairs_surfaces_dicts = vol_consumer.get_last_vols()
    for ccypair in ccypairs_surfaces_dicts:
        ccypair_surface_dict = ccypairs_surfaces_dicts[ccypair]
        ccypair_surface = vs.VolatilitySurface.from_dict(ccypair_surface_dict)
        print(ccypair_surface)


def try_rfq_vols_KafkaConsumer(kbs: KafkaBootstrapServer, ccy_pair_list: List[str], vol_src: VolSource):
    rfq_vols_consumer = RfqVolKafkaConsumer(kbs, ccy_pair_list, vol_src)
    ccypairs_surfaces_dicts = rfq_vols_consumer.get_last_rfq_vols()
    for ccypair in ccypairs_surfaces_dicts:
        ccypair_surface_dict = ccypairs_surfaces_dicts[ccypair]
        ccypair_surface = vs.VolatilitySurface.from_dict(ccypair_surface_dict)
        print(ccypair_surface)


def try_rfq_tenordates_KafkaConsumer(kbs: KafkaBootstrapServer, ccy_pair: str):
    rfq_vols_consumer = TenorDatesKafkaConsumer(kbs, ccy_pair_list)
    ccypair_tenordates_dict = rfq_vols_consumer.get_last_tenordates()
    print(ccypair_tenordates_dict)


def try_spots_KafkaConsumer(kbs: KafkaBootstrapServer, spot_src: SpotSource, ccy_pair_list: List[str] = None):
    spot_consumer = SpotKafkaConsumer(kbs, spot_src)
    spots_dict = spot_consumer.get_last_spots()
    print(spots_dict)
    print(spots_dict['quotes'])

    if ccy_pair_list:
        ccypairs_spots = [spot_quote for spot_quote in spots_dict['quotes']
                          if spot_quote['ccy_pair'] in ccy_pair_list]
        ccypairs_spots_dict = {}
        for ccypair_spot in ccypairs_spots:
            ccypairs_spots_dict[ccypair_spot['ccy_pair']] = ccypair_spot
        for ccy_pair in ccy_pair_list:
            ask = ccypairs_spots_dict[ccy_pair]['ask']
            bid = ccypairs_spots_dict[ccy_pair]['bid']
            mid = ccypairs_spots_dict[ccy_pair]['mid']
            print('{ccypair} - ask: {ask}, bid: {bid}, mid: {mid}'.format(ccypair=ccy_pair, ask=ask, bid=bid, mid=mid))


def try_mksys_latest_marketstate():
    marketstate_dict = mksysMktPub._get_latest_marketstate_from_mksys()
    _logger.info(marketstate_dict['data']['volatility_surfaces']['EURUSD'])


if __name__ == '__main__':
    # try_reading_ccypairs_config()
    # try_rfq_tenordates_KafkaConsumer()
    # try_mksys_latest_marketstate()
    # try_spots_KafkaConsumer(KafkaBootstrapServer.wmc_cp_vm09, SpotSource.fenics, ccy_pair_list)
    # try_timezone()
    # try_arrow()
    # try_vols_KafkaConsumer(Source.jpm, 'EURUSD')
    # try_rfq_tenordates_KafkaConsumer('EURUSD')
    try_rfq_vols_KafkaConsumer(KafkaBootstrapServer.wmc_cp_vm09, ['EURUSD'], VolSource.jpm)
