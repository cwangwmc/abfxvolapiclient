from volatilitySurface import *
from abfxVolResponseXmlParser import abfxVolConventionsHelper


import unittest
import abfxVolResponseXmlParser as parser
import datetime as dt

_dbabfx_xml_samples_dir = './dbabfx_xml_samples/'


class TestAbfxRespXmlParserEURUSD(unittest.TestCase):

    def setUp(self):

        self._xml_resp_EURUSD_filename = _dbabfx_xml_samples_dir + 'sample_EURUSD.xml'
        self._eurusd_vol_surface = parser.abfx_vol_surface_from_xml(self._xml_resp_EURUSD_filename, True)

    def test_eurusd_surface_constructs(self):

        self.assertIsNotNone(self._eurusd_vol_surface)
        self.assertEqual(self._eurusd_vol_surface.SecuritySymbol, 'EURUSD')
        self.assertIsNotNone(self._eurusd_vol_surface.AsOfTimeStamp)
        self.assertIsInstance(self._eurusd_vol_surface.AsOfTimeStamp, dt.datetime)
        self.assertIsNotNone(self._eurusd_vol_surface.VolSmiles)
        self.assertEqual(len(self._eurusd_vol_surface.VolSmiles), 11)
        self.assertEqual(len(self._eurusd_vol_surface.VolSmileTenors), 11)

    def test_eurusd_surface_has_atm_vol_pt_in_each_smile(self):

        for vol_smile in self._eurusd_vol_surface.VolSmiles:
            self.assertIsNotNone(vol_smile.AtmVolPoint)
            self.assertIsInstance(vol_smile.AtmVolPoint.AtmType, Atm)
            self.assertEqual(vol_smile.AtmVolPoint.IsAtm, True)
            self.assertEqual(vol_smile.AtmVolPoint.AtmType, Atm.Dn)

            self.assertIsNotNone(vol_smile.AtmVolPoint.VolInDecimal)
            self.assertIsNotNone(vol_smile.AtmVolPoint.VolInPercentage)
            self.assertGreater(vol_smile.AtmVolPoint.VolInDecimal, 0.0)
            self.assertGreater(vol_smile.AtmVolPoint.VolInPercentage, 0.0)

            self.assertIsNone(vol_smile.AtmVolPoint.DeltaDetails)
            self.assertIsNone(vol_smile.AtmVolPoint.DeltaAsDecimal)
            self.assertIsNone(vol_smile.AtmVolPoint.DeltaAsInt)
            self.assertIsNone(vol_smile.AtmVolPoint.DeltaForCallOrPut)
            self.assertTrue(not isinstance(vol_smile.AtmVolPoint.AtmType, DeltaType))

    def test_eurusd_surface_has_10_25_delta_vol_pts_in_each_smile(self):

        for vol_smile in self._eurusd_vol_surface.VolSmiles:
            delta_vol_pts = [vol_pt for vol_pt in vol_smile.VolSmilePoints if vol_pt.DeltaDetails is not None]
            self.assertGreater(len(delta_vol_pts), 0)
            self.assertEqual(len(delta_vol_pts), 4)

            delta_25_vol_pts = [vol_pt for vol_pt in delta_vol_pts if vol_pt.DeltaAsInt == 25]
            delta_10_vol_pts = [vol_pt for vol_pt in delta_vol_pts if vol_pt.DeltaAsInt == 10]
            self.assertGreater(len(delta_25_vol_pts), 0)
            self.assertGreater(len(delta_10_vol_pts), 0)
            self.assertEqual(len(delta_25_vol_pts), 2)
            self.assertEqual(len(delta_10_vol_pts), 2)

            delta_025_vol_pts = [vol_pt for vol_pt in delta_vol_pts if vol_pt.DeltaAsDecimal == 0.25]
            delta_010_vol_pts = [vol_pt for vol_pt in delta_vol_pts if vol_pt.DeltaAsDecimal == 0.10]
            self.assertGreater(len(delta_025_vol_pts), 0)
            self.assertGreater(len(delta_010_vol_pts), 0)
            self.assertEqual(len(delta_025_vol_pts), 2)
            self.assertEqual(len(delta_010_vol_pts), 2)

            delta_25_vol_pts = vol_smile.vol_smile_points_for_int_delta(25)
            delta_10_vol_pts = vol_smile.vol_smile_points_for_int_delta(10)
            self.assertGreater(len(delta_25_vol_pts), 0)
            self.assertGreater(len(delta_10_vol_pts), 0)
            self.assertEqual(len(delta_25_vol_pts), 2)
            self.assertEqual(len(delta_10_vol_pts), 2)

            delta_025_vol_pts = vol_smile.vol_smile_points_for_decimal_delta(0.25)
            delta_010_vol_pts = vol_smile.vol_smile_points_for_decimal_delta(0.10)
            self.assertGreater(len(delta_025_vol_pts), 0)
            self.assertGreater(len(delta_010_vol_pts), 0)
            self.assertEqual(len(delta_025_vol_pts), 2)
            self.assertEqual(len(delta_010_vol_pts), 2)

    def test_eurusd_surface_has_put_call_vol_pts_in_each_smile(self):

        for vol_smile in self._eurusd_vol_surface.VolSmiles:
            delta_vol_pts = [vol_pt for vol_pt in vol_smile.VolSmilePoints if vol_pt.DeltaDetails is not None]
            call_delta_vol_pts = [vol_pt for vol_pt in delta_vol_pts if vol_pt.DeltaForCallOrPut == DeltaOrientation.Call]
            put_delta_vol_pts = [vol_pt for vol_pt in delta_vol_pts if vol_pt.DeltaForCallOrPut == DeltaOrientation.Put]
            self.assertGreater(len(call_delta_vol_pts), 0)
            self.assertGreater(len(put_delta_vol_pts), 0)
            self.assertEqual(len(call_delta_vol_pts), 2)
            self.assertEqual(len(put_delta_vol_pts), 2)

            delta_10_call_vol_pt = [vol_pt for vol_pt in call_delta_vol_pts if vol_pt.DeltaAsInt == 10]
            delta_25_call_vol_pt = [vol_pt for vol_pt in call_delta_vol_pts if vol_pt.DeltaAsInt == 25]
            delta_10_put_vol_pt = [vol_pt for vol_pt in put_delta_vol_pts if vol_pt.DeltaAsInt == 10]
            delta_25_put_vol_pt = [vol_pt for vol_pt in put_delta_vol_pts if vol_pt.DeltaAsInt == 25]
            self.assertGreater(len(delta_25_call_vol_pt), 0)
            self.assertEqual(len(delta_25_call_vol_pt), 1)
            self.assertGreater(len(delta_10_call_vol_pt), 0)
            self.assertEqual(len(delta_10_call_vol_pt), 1)

            self.assertGreater(len(delta_25_put_vol_pt), 0)
            self.assertEqual(len(delta_10_put_vol_pt), 1)
            self.assertGreater(len(delta_10_put_vol_pt), 0)
            self.assertEqual(len(delta_10_put_vol_pt), 1)

    def test_eurusd_bp_delta_across_entire_surface(self):

        for vol_smile in self._eurusd_vol_surface.VolSmiles:
            delta_vol_pts = vol_smile.DeltaVolPoints
            self.assertGreater(len(delta_vol_pts), 0)
            for vol_pt in delta_vol_pts:
                self.assertTrue(vol_pt.DeltaType is DeltaType.SpotBp or vol_pt.DeltaType.ForwardBp)

    def test_eurusd_spot_delta_below_2Y_tenor(self):

        all_tenors = self._eurusd_vol_surface.VolSmileTenors
        tenor_2y = Tenor('2Y')
        below_2y_tenors = [t for t in all_tenors if Tenor(t) < tenor_2y]
        for t in below_2y_tenors:
            vol_smile = self._eurusd_vol_surface.vol_smile_for_tenor(t)
            delta_vol_pts = [vol_pt for vol_pt in vol_smile.VolSmilePoints if not vol_pt.IsAtm]
            for vol_pt in delta_vol_pts:
                self.assertEqual(vol_pt.DeltaType, DeltaType.SpotBp)

    def test_eurusd_fwd_delta_from_2Y_tenor_and_beyond(self):

        all_tenors = self._eurusd_vol_surface.VolSmileTenors
        tenor_2y = Tenor('2Y')
        beyond_2y_tenors = [t for t in all_tenors if Tenor(t) >= tenor_2y]
        for t in beyond_2y_tenors:
            vol_smile = self._eurusd_vol_surface.vol_smile_for_tenor(t)
            delta_vol_pts = [vol_pt for vol_pt in vol_smile.VolSmilePoints if not vol_pt.IsAtm]
            for vol_pt in delta_vol_pts:
                self.assertEqual(vol_pt.DeltaType, DeltaType.ForwardBp)


class TestAbfxRespXmlParserUSDTRY(unittest.TestCase):

    def setUp(self):

        self._xml_resp_USDTRY_filename = _dbabfx_xml_samples_dir + 'sample_USDTRY.xml'
        self._usdtry_vol_surface = parser.abfx_vol_surface_from_xml(self._xml_resp_USDTRY_filename, True)

    def test_usdtry_surface_constructs(self):

        self.assertIsNotNone(self._usdtry_vol_surface)
        self.assertEqual(self._usdtry_vol_surface.SecuritySymbol, 'USDTRY')
        self.assertIsNotNone(self._usdtry_vol_surface.AsOfTimeStamp)
        self.assertIsInstance(self._usdtry_vol_surface.AsOfTimeStamp, dt.datetime)
        self.assertIsNotNone(self._usdtry_vol_surface.VolSmiles)
        self.assertEqual(len(self._usdtry_vol_surface.VolSmiles), 10)
        self.assertEqual(len(self._usdtry_vol_surface.VolSmileTenors), 10)

    def test_usdtry_fwd_pc_delta_across_entire_surface(self):

        delta_vol_pts = [vol_pt for vol_smile in self._usdtry_vol_surface.VolSmiles for vol_pt in vol_smile.VolSmilePoints
                         if not vol_pt.IsAtm]
        self.assertTrue(all([vol_pt.DeltaType == DeltaType.ForwardPc for vol_pt in delta_vol_pts]))


class TestAbfxRespXmlParserEURXXX(unittest.TestCase):

    def setUp(self):

        self._xml_resp_EURXXX_with_error_filename = _dbabfx_xml_samples_dir + 'sample_EURXXX_error.xml'

    def test_parser_raises_value_error_xml_with_error(self):

        self.assertRaises(ValueError, parser.abfx_vol_surface_from_xml,
                          self._xml_resp_EURXXX_with_error_filename, True)

        expected_vol_surfce = None
        try:
            expected_vol_surfce = parser.abfx_vol_surface_from_xml(self._xml_resp_EURXXX_with_error_filename, True)
        except ValueError as err:
            self.assertIsNotNone(err)
        self.assertIsNone(expected_vol_surfce)


class TestAbfxVolConventionsHelper(unittest.TestCase):

    def setUp(self):
        self._abfx_vol_conv_helper = abfxVolConventionsHelper()

    def test_eurusd_vol_convention(self):
        eurusd_vol_conv = self._abfx_vol_conv_helper.abfx_vol_convention('EURUSD')
        self.assertEqual(eurusd_vol_conv, ('EURUSD', 'FORWARDRISK', '2Y'))

    def test_usdjpy_vol_convention(self):
        usdjpy_vol_conv = self._abfx_vol_conv_helper.abfx_vol_convention('USDJPY')
        self.assertEqual(usdjpy_vol_conv, ('USDJPY', 'FENICS', '2Y'))

    def test_eurusd_is_spot_bp_delta_below_2Y(self):
        self.assertEqual(self._abfx_vol_conv_helper.abfx_delta_type('EURUSD', tenor='1Y'), DeltaType.SpotBp)

    def test_eurusd_is_fwd_bp_delta_from_2Y(self):
        self.assertEqual(self._abfx_vol_conv_helper.abfx_delta_type('EURUSD', tenor='2Y'), DeltaType.ForwardBp)

    def test_usdjpy_is_spot_pc_delta_below_2Y(self):
        self.assertEqual(self._abfx_vol_conv_helper.abfx_delta_type('USDJPY', tenor='6M'), DeltaType.SpotPc)

    def test_usdjpy_is_fwd_pc_delta_from_2Y(self):
        self.assertEqual(self._abfx_vol_conv_helper.abfx_delta_type('USDJPY', tenor='5Y'), DeltaType.ForwardPc)

    def test_usdtry_is_fwd_pc_delta_from_overnight(self):
        self.assertEqual(self._abfx_vol_conv_helper.abfx_delta_type('USDTRY', tenor='ON'), DeltaType.ForwardPc)
        self.assertEqual(self._abfx_vol_conv_helper.abfx_delta_type('USDTRY', tenor='1D'), DeltaType.ForwardPc)
