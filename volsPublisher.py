from requests import Session, Request
from typing import *
from volatilitySurface import *
from tenorDates import *
from kafkaConsumers import SpotSource, VolSource, KafkaBootstrapServer, \
    VolKafkaConsumer, RfqVolKafkaConsumer, TenorDatesKafkaConsumer, SpotKafkaConsumer

import myconfigs
import abfxVolResponseXmlParser as abfxVolXmlParser
import requests
import threading
import logging
import time
import copy
import arrow


_logger = logging.getLogger()

_mksys_GET_mkt_end_point = 'https://trade.mksys.net/api/market/v1/marketstates/latest'
_mksys_PUT_mkt_end_point = 'https://trade.mksys.net/api/market/v1/marketstates/'


def _overwrite_marketstate_with_vol_surface(marketstate_data_dict: dict, vol_surface: VolatilitySurface,
                                            tenor_dates: TenorDates = None):
    sec_sym = vol_surface.SecuritySymbol
    vol_surfaces_dict = marketstate_data_dict['volatility_surfaces']
    if sec_sym in vol_surfaces_dict:
        vol_surface_ref_date = vol_surface.ReferenceDate
        marketstate_vol_surface_ref_date = arrow.get(vol_surfaces_dict[sec_sym]['ref_date']).datetime

        if (vol_surface_ref_date.date() == marketstate_vol_surface_ref_date.date()
                or tenor_dates):
            vol_surface_overlay = _encode_volatility_surface(vol_surface,
                                                             vol_surfaces_dict[sec_sym], tenor_dates)
            _logger.debug('{} vol surface overlay:\n{}'.format(sec_sym, vol_surface_overlay))
            vol_surfaces_dict[sec_sym] = vol_surface_overlay
        else:
            _logger.warning('{0} Ref Date Discrepancy - Vol Surface Ref Date: {1} - Marketstate Ref Date: {2}.'
                            .format(sec_sym, vol_surface.ReferenceDate.strftime('%Y-%m-%d'),
                                    arrow.get(vol_surfaces_dict[sec_sym]['ref_date']).datetime.strftime('%Y-%m-%d')))
            _logger.warning('Skip overwriting...')

    else:
        _logger.warning('{0} is not found in marketstate.'.format(sec_sym))


def _overwrite_marketstate_with_latest_spots(marketstate_data_dict: dict, spots_dict: dict):

    spots_quotes = spots_dict['quotes']

    spots_quotes_tuples = [(q['ccy_pair'], q['mid']) for q in spots_quotes]

    spot_lvls_dict = {}
    for qt_ccy_pair, qt_mid in spots_quotes_tuples:
        spot_lvls_dict[qt_ccy_pair] = qt_mid

    marketstate_data_dict['spot_levels'] = spot_lvls_dict


def _get_latest_marketstate(session: Session, retries_if_fails: int = 3) -> dict:
    mksys_GET_end_point = _mksys_GET_mkt_end_point

    marketstate_dict = None
    timeout_in_secs = 30
    try_count = 1

    while marketstate_dict is None:

        get_req = None

        try:

            _logger.info('Making GET request to end point {url} for the {n} time(s) ...'
                         .format(url=mksys_GET_end_point, n=try_count))
            get_req = session.request(method='GET', url=mksys_GET_end_point,
                                      headers={'Accept': 'application/json'}, timeout=timeout_in_secs)
            _logger.info('GET request has returned with status code {0}.\n'.format(get_req.status_code))
            _logger.debug('GET request has returned with text content:\n{0}\n'.format(get_req.text))
            marketstate_dict = json.loads(get_req.text)
            _logger.debug('GET request returned json text content has been loaded to be {0}'.format(marketstate_dict))
            get_req.close()

        except requests.exceptions.Timeout:
            _logger.warn('GET request has timed out after {} secs ... Will close the request and re-try ...'
                         .format(timeout_in_secs))
            if get_req:
                get_req.close()

        except Exception as exp:
            _logger.error('Exception ({0}) occured while trying to make GET request to {1}.'
                          .format(exp, mksys_GET_end_point))

    return marketstate_dict


def _put_overwritten_marketstate(session: Session, source: VolSource, marketstate: dict, retries_if_fails: int = 3):
    mksys_PUT_end_point = _mksys_PUT_mkt_end_point

    arw_utc_now = arrow.get(dt.datetime.utcnow().replace(microsecond=0)).isoformat()
    marketstate['data']['asof_date'] = arw_utc_now
    marketstate_data_json = json.dumps(marketstate['data'])

    mkt_src = ''
    if (source == VolSource.dbabfx):
        mkt_src = 'db-abfx'
    else:
        mkt_src = source.name

    post_data = {
                'asof_date': arw_utc_now,
                'market_source': mkt_src,
                'data': marketstate_data_json,
                'raw': json.dumps({})
                }

    has_published = False
    timeout_in_secs = 30
    try_count = 1

    while not has_published:

        put_req = None

        try:

            _logger.info('Making PUT request to end point {url} for the {n} time(s) ...'
                         .format(url=mksys_PUT_end_point, n=try_count))

            put_req = session.request(method='PUT', url=mksys_PUT_end_point, data=post_data, timeout=timeout_in_secs)
            if put_req and put_req.status_code == 200:
                _logger.debug(put_req.text)
                put_req.close()
                has_published = True

        except requests.exceptions.Timeout:
            _logger.warn('PUT request has timed out after {} secs ... Will close the request and re-try ...'
                         .format(timeout_in_secs))
            has_published = False
            if put_req:
                put_req.close()

        except Exception as exp:
            _logger.error('Exception ({0}) occurred while trying to make PUT request to {1}.'
                          .format(exp, mksys_PUT_end_point))
            raise

    _logger.info('PUT request has returned with status code {0}.\n'.format(put_req.status_code))
    return


def _encode_volatility_surface(vol_surface: VolatilitySurface,
                               origin_vol_surface_template: dict,
                               tenor_dates: TenorDates = None) -> dict:

    vol_surface_overlay = copy.deepcopy(origin_vol_surface_template)
    origin_dates_list = origin_vol_surface_template['curves']['atm']['dates']
    origin_tenors_list = origin_vol_surface_template['curves']['atm']['tenors']

    sec_sym = vol_surface.SecuritySymbol
    # Exluding Overnight Tenor
    vol_surface_tenors_list = [tnr for tnr in vol_surface.VolSmileTenors if tnr not in ['ON', '1D']]

    _1W_smile = vol_surface.vol_smile_for_tenor('1W')
    _1W_smile_25d = _1W_smile.vol_smile_points_for_int_delta(25)[0]
    is_1W_forward = 'Forward' in _1W_smile_25d.DeltaType.name
    _logger.debug('{} - Vol Surface Forward {}'.format(sec_sym, is_1W_forward))

    tenor_dates_list = []
    tenors_list = []
    types_list = []
    atm_vols_in_decimal = []
    d10c_vols_in_decimal = []
    d25c_vols_in_decimal = []
    d25p_vols_in_decimal = []
    d10p_vols_in_decimal = []

    if tenor_dates:
        _logger.info('Tenor Dates are present, will use the maturity dates for matching tenors.')
        for tnr in vol_surface_tenors_list:
            maturity_date = tenor_dates.GetMaturityDate(tnr)
            if maturity_date:
                maturity_date_iso = arrow.get(maturity_date).isoformat()
                _logger.info('From Tenor Dates: {0} - {1} - {2}'.format(sec_sym, tnr, maturity_date_iso))
                tenors_list.append(tnr)
                tenor_dates_list.append(maturity_date_iso)
            else:
                _logger.warning('RFQ Tenor Dates: No maturity date is found for tenor {0} in vol surface {1}, will exclude.'
                                .format(tnr, sec_sym))
    else:
        _logger.info('No Tenor Dates, will use the origin maturity dates.')
        index = 0
        for tnr in origin_tenors_list:
            if tnr in vol_surface_tenors_list:
                origin_tnr_maturity_date_str = origin_dates_list[index]
                _logger.info('From origin: {0} - {1} - {2}'.format(sec_sym, tnr, origin_tnr_maturity_date_str))
                tenors_list.append(tnr)
                tenor_dates_list.append(origin_tnr_maturity_date_str)
            else:
                _logger.warning('Origin: No tenor {0} found in vol surface {1}, will exclude.'.format(tnr, sec_sym))
            index += 1

    index = 0
    for t in tenors_list:
        types_list.append('CurvePointType.RELATIVE_DATE')
        vol_smile_t = vol_surface.vol_smile_for_tenor(t)
        atm_vols_in_decimal.append(vol_smile_t.AtmVolPoint.VolInDecimal)
        d10c_vols_in_decimal.append([vol_pt for vol_pt in vol_smile_t.vol_smile_points_for_int_delta(10)
                                     if vol_pt.DeltaForCallOrPut == DeltaOrientation.Call][0].VolInDecimal)
        d10p_vols_in_decimal.append([vol_pt for vol_pt in vol_smile_t.vol_smile_points_for_int_delta(10)
                                     if vol_pt.DeltaForCallOrPut == DeltaOrientation.Put][0].VolInDecimal)
        d25c_vols_in_decimal.append([vol_pt for vol_pt in vol_smile_t.vol_smile_points_for_int_delta(25)
                                     if vol_pt.DeltaForCallOrPut == DeltaOrientation.Call][0].VolInDecimal)
        d25p_vols_in_decimal.append([vol_pt for vol_pt in vol_smile_t.vol_smile_points_for_int_delta(25)
                                     if vol_pt.DeltaForCallOrPut == DeltaOrientation.Put][0].VolInDecimal)

    d10c_curve = {'curve_type': 'VolCurve',
                  'dates': tenor_dates_list,
                  'tenors': tenors_list,
                  'types': types_list,
                  'values': d10c_vols_in_decimal}

    d10p_curve = {'curve_type': 'VolCurve',
                  'dates': tenor_dates_list,
                  'tenors': tenors_list,
                  'types': types_list,
                  'values': d10p_vols_in_decimal}

    atm_curve = {'curve_type': 'VolCurve',
                 'dates': tenor_dates_list,
                 'tenors': tenors_list,
                 'types': types_list,
                 'values': atm_vols_in_decimal}

    d25c_curve = {'curve_type': 'VolCurve',
                  'dates': tenor_dates_list,
                  'tenors': tenors_list,
                  'types': types_list,
                  'values': d25c_vols_in_decimal}

    d25p_curve = {'curve_type': 'VolCurve',
                  'dates': tenor_dates_list,
                  'tenors': tenors_list,
                  'types': types_list,
                  'values': d25p_vols_in_decimal}

    vol_surface_overlay['curves']['10c'] = d10c_curve
    vol_surface_overlay['curves']['10p'] = d10p_curve
    vol_surface_overlay['curves']['25c'] = d25c_curve
    vol_surface_overlay['curves']['25p'] = d25p_curve
    vol_surface_overlay['curves']['atm'] = atm_curve
    vol_surface_overlay['surface_is_forward'] = is_1W_forward
    vol_surface_overlay['surface_is_forward_past_1y'] = is_1W_forward

    if vol_surface.ReferenceSpot:
        vol_surface_overlay['ref_spot'] = vol_surface.ReferenceSpot

    if vol_surface.ReferenceDate:
        vol_surface_overlay['ref_date'] = vol_surface.ReferenceDateIso

    return vol_surface_overlay


class VolsPublisher(threading.Thread):

    _stop_event = None
    _ccy_pairs = []
    _pause_interval_in_seconds = None
    _origin_session = None
    _vol_kfc = None
    _td_kfc = None
    _spt_kfc = None
    _rfq_vol_kfc = None

    def __init__(self, ccy_pairs: List[str], vol_src: VolSource, spot_src: SpotSource,
                 bootstrap_server: KafkaBootstrapServer,
                 pause_interval_in_seconds: int,
                 include_extra_tenor_vols: bool = False):
        threading.Thread.__init__(self)
        self._stop_event = threading.Event()
        self._ccy_pairs = ccy_pairs
        self._pause_interval_in_seconds = pause_interval_in_seconds
        self._origin_session = Session()
        self._origin_session.cert = myconfigs.current_file_dir_path() + myconfigs.alice_cert_filename
        self._vol_kfc = VolKafkaConsumer(bootstrap_server, ccy_pairs, vol_src)
        self._td_kfc = TenorDatesKafkaConsumer(bootstrap_server, ccy_pairs)
        self._spt_kfc = SpotKafkaConsumer(bootstrap_server, spot_src)
        if (include_extra_tenor_vols):
            self._rfq_vol_kfc = RfqVolKafkaConsumer(bootstrap_server, ccy_pairs, vol_src)

    def run(self):
        self._gather_overwrite_and_publish()
        while not self._stop_event.wait(self._pause_interval_in_seconds):
            self._gather_overwrite_and_publish()

    def stop(self):
        self._stop_event.set()

    def _gather_overwrite_and_publish(self):

        marketstate_dict = _get_latest_marketstate(self._origin_session)
        _logger.debug('Latest marketstate:\n{0}'.format(marketstate_dict))
        marketstate_data_dict = marketstate_dict['data']

        ccypairs_vol_surfaces_dict = self._vol_kfc.get_last_vols()
        ccypairs_tenordates_dict = self._td_kfc.get_last_tenordates()
        spots_dict = self._spt_kfc.get_last_spots()

        ccypairs_vol_surfs_extra_tnrs_dict = None
        if (self._rfq_vol_kfc):
            ccypairs_vol_surfs_extra_tnrs_dict = self._rfq_vol_kfc.get_last_rfq_vols()

        for currency_pair, vol_surface_dict in ccypairs_vol_surfaces_dict.items():
            vol_surface = VolatilitySurface.from_dict(vol_surface_dict)

            _logger.info('Vol Surface {0}, As Of: {1}, Ref Date: {2}, Ref Spot: {3}.'.format(
                         vol_surface.SecuritySymbol, str(vol_surface.AsOfTimeStamp),
                         vol_surface.ReferenceDate.strftime('%Y-%m-%d'), str(vol_surface.ReferenceSpot)))

            _logger.debug(vol_surface.to_dict())

            if ccypairs_vol_surfs_extra_tnrs_dict:
                if currency_pair in ccypairs_vol_surfs_extra_tnrs_dict:
                    vol_surf_extra_tnrs_dict = ccypairs_vol_surfs_extra_tnrs_dict[currency_pair]
                    vol_surface_extra_tnrs = VolatilitySurface.from_dict(vol_surf_extra_tnrs_dict)
                    asof_diff = abs(vol_surface.AsOfTimeStamp - vol_surface_extra_tnrs.AsOfTimeStamp)
                    asof_diff_mins = round(asof_diff.total_seconds() / 60.0, 2)
                    _logger.info('As Of Diff: {0} min(s)'.format(asof_diff_mins))

                    if (vol_surface.ReferenceDate.date() == vol_surface_extra_tnrs.ReferenceDate.date()
                       and asof_diff_mins < 30.0):

                        _logger.info('Vol Surface Extra Tenors {0}, As Of: {1}, Ref Date: {2}'.format(
                                    vol_surface_extra_tnrs.SecuritySymbol, str(vol_surface_extra_tnrs.AsOfTimeStamp),
                                    vol_surface_extra_tnrs.ReferenceDate.strftime('%Y-%m-%d')))

                        vol_surface.try_add_vol_smiles(vol_surface_extra_tnrs.VolSmiles)

            tenor_dates = None
            if currency_pair in ccypairs_tenordates_dict:
                tenor_dates_candidate = TenorDates.from_dict(ccypairs_tenordates_dict[currency_pair])
                tenor_dates_refdate = tenor_dates_candidate.ReferenceDate
                vol_surface_refdate = vol_surface.ReferenceDate
                if (tenor_dates_refdate.date() == vol_surface_refdate.date()):
                    tenor_dates = tenor_dates_candidate

            _overwrite_marketstate_with_vol_surface(marketstate_data_dict, vol_surface, tenor_dates)

        _overwrite_marketstate_with_latest_spots(marketstate_data_dict, spots_dict)

        marketstate_dict['data'] = marketstate_data_dict
        _put_overwritten_marketstate(self._origin_session, self._vol_kfc.Source, marketstate_dict)
