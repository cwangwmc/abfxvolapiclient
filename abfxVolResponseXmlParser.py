from volatilitySurface import *
from typing import *
from dateutil import tz

import myconfigs
import xml.etree.ElementTree as et
import datetime as dt
import logging

_logger = logging.getLogger()


class abfxVolConventionsHelper:

    _bp_indicator = 'FORWARDRISK'
    _pc_indicator = 'FENICS'
    _abfx_vol_conv_cnfg_path_filename = myconfigs.current_file_dir_path() + myconfigs.abfx_vol_convention_config_filename

    def __init__(self, abfx_vol_conventions_config_filename: str = _abfx_vol_conv_cnfg_path_filename) -> None:
        _logger.debug('Reading abfx vol conventions from config file {0}.'.format(abfx_vol_conventions_config_filename))
        vol_conv_config_file = open(abfx_vol_conventions_config_filename, 'r')
        vol_convention_config_lines = vol_conv_config_file.readlines()
        vol_conv_config_file.close()
        # Getting rid of the trailing line breaks
        vol_conventions_config_no_line_breaks = [l[:-1] for l in vol_convention_config_lines]
        # Splitting each line into a list of Tuples
        # E.g. ('EURUSD', 'FORWRADRISK', '2Y') ('USDJPY', 'FENICS', '2Y')
        vol_conventions_config_tuples = [(l.split(';')[0], l.split(';')[1].split('-')[0], l.split(';')[1].split('-')[1])
                                         for l in vol_conventions_config_no_line_breaks]

        self._abfx_vol_conventions = []
        for ccypair, bp_pc, spot_delta_cutoff_tenor in vol_conventions_config_tuples:
            if [(x, y, z) for (x, y, z) in self._abfx_vol_conventions if x == ccypair.upper()]:
                _logger.warn('Currency pair {0} duplicate detected. Discarding...'.format(ccypair))
                continue

            if bp_pc != abfxVolConventionsHelper._bp_indicator and \
                    bp_pc != abfxVolConventionsHelper._pc_indicator:
                _logger.warn('Unrecognised Bp Pc delta indicator {0} detected. Discarding...'.format(bp_pc))
                continue

            self._abfx_vol_conventions.append((ccypair.upper(), bp_pc.upper(), spot_delta_cutoff_tenor.upper()))

    def abfx_vol_convention(self, ccypair: str) -> Tuple[str, str, str]:
        abfx_vol_conv_ccypair = [(x, y, z) for (x, y, z) in self._abfx_vol_conventions if x == ccypair.upper()]
        if len(abfx_vol_conv_ccypair) > 0:
            return abfx_vol_conv_ccypair[0]

    def abfx_delta_type(self, ccypair: str, tenor: str) -> DeltaType:
        convention_tuple = self.abfx_vol_convention(ccypair)
        if convention_tuple:
            delta_type_indicator = convention_tuple[1]
            spot_delta_cutoff_tenor = Tenor(convention_tuple[2])
            tenor_in_question = Tenor(tenor)
            if (delta_type_indicator == abfxVolConventionsHelper._bp_indicator):
                if (tenor_in_question < spot_delta_cutoff_tenor):
                    return DeltaType.SpotBp
                else:
                    return DeltaType.ForwardBp
            elif (delta_type_indicator == abfxVolConventionsHelper._pc_indicator):
                if (tenor_in_question < spot_delta_cutoff_tenor):
                    return DeltaType.SpotPc
                else:
                    return DeltaType.ForwardPc


_abfx_vol_conv_helper = None


def _global_abfx_vol_conv_helper() -> 'abfxVolConventionsHelper':
    global _abfx_vol_conv_helper
    if _abfx_vol_conv_helper is None:
        _logger.debug('Initialising abfx vol conentions helper...')
        _abfx_vol_conv_helper = abfxVolConventionsHelper()
    return _abfx_vol_conv_helper


def abfx_vol_surface_from_xml(abfxVolXml: str, is_filename: bool = False) -> Union[VolatilitySurface, None]:

    abfx_vol_conv_helper = _global_abfx_vol_conv_helper()

    abfxVolXml_tree_root = None
    if (is_filename):
        abfxVolXml_tree_root = et.parse(abfxVolXml).getroot()
    else:
        abfxVolXml_tree_root = et.fromstring(abfxVolXml)

    potential_error_nodes = abfxVolXml_tree_root.findall('.//error')
    if (potential_error_nodes):
        flattened_error_msg = ''
        for error_node in potential_error_nodes:
            flattened_error_msg += '{0}\n'.format(error_node.text)
        raise ValueError(flattened_error_msg)

    currencypair_node = abfxVolXml_tree_root

    currencypair = currencypair_node.attrib['value']
    quote_status = currencypair_node.attrib['quote_status']
    asof_timestamp_naive = dt.datetime.strptime(currencypair_node.attrib['price_time'], '%d %b %Y %X')
    asof_timestamp_aware = arrow.get(asof_timestamp_naive, tz.gettz('Europe/London')).to('utc').datetime
    ref_date_no_timestamp = dt.datetime.strptime(asof_timestamp_aware.strftime('%Y-%m-%d'), '%Y-%m-%d')

    vol_smiles = []
    for tenor_node in currencypair_node:
        tenor_vol_smile = _abfx_vol_construct_tenor_volatility_smile(tenor_node, currencypair)
        vol_smiles.append(tenor_vol_smile)

    vol_surface = VolatilitySurface(securitySymbol=currencypair, asOfTimestamp=asof_timestamp_aware, volSmiles=vol_smiles,
                                    source='dbabfx', reference_date=ref_date_no_timestamp)

    return vol_surface


def _abfx_vol_construct_tenor_volatility_smile(abfxVolTenorNode: et.Element, ccypair: str) -> VolatilitySmile:

    tenor = abfxVolTenorNode.attrib['value']
    vol_smile_points = []
    for vol_point_node in abfxVolTenorNode:
        vol_sm_pt = _abfx_vol_construct_volatility_smile_point(vol_point_node, ccypair, tenor)
        vol_smile_points.append(vol_sm_pt)

    vol_smile = VolatilitySmile(tenor=tenor, volSmilePoints=vol_smile_points)

    return vol_smile


def _abfx_vol_construct_volatility_smile_point(abfxVolPointNode: et.Element,
                                               ccypair: str, tenor: str) -> VolatilitySmilePoint:

    abfx_vol_conv_helper = _global_abfx_vol_conv_helper()
    delta = abfxVolPointNode.attrib['delta']
    delta_option_type = abfxVolPointNode.attrib['option_type'].upper()
    vol_in_percentage = float(abfxVolPointNode.attrib['value'])

    # Initialise as ATM Delta Neutrol
    # Reassign with the correct vol smile point location info if it turns out not to be ATM
    vol_smile_pt_loc = Atm.Dn

    if (delta != 'neutral'):
        delta_type = abfx_vol_conv_helper.abfx_delta_type(ccypair=ccypair, tenor=tenor)
        delta_orientation = None
        if delta_option_type == 'CALL':
            delta_orientation = DeltaOrientation.Call
        elif delta_option_type == 'PUT':
            delta_orientation = DeltaOrientation.Put
        vol_smile_pt_loc = (int(delta), delta_orientation, delta_type)

    vol_smile_point = VolatilitySmilePoint(volInPercentage=vol_in_percentage, volSmilePtLoc=vol_smile_pt_loc)

    return vol_smile_point
