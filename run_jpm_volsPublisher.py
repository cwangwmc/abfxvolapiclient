from kafkaConsumers import VolSource, SpotSource, KafkaBootstrapServer
from volsPublisher import VolsPublisher

import myconfigs
import threading
import logging
import logging.handlers
import time
import os

_current_file_dir_path = myconfigs.current_file_dir_path()

_debug_logs_dir = '_logs.jpm.debug/'
_logs_dir = '_logs.jpm/'

_debug_logs_path = _current_file_dir_path + _debug_logs_dir
_logs_path = _current_file_dir_path + _logs_dir

if not os.path.exists(_debug_logs_path):
    os.makedirs(_debug_logs_path)

if not os.path.exists(_logs_path):
    os.makedirs(_logs_path)

_logger = logging.getLogger()
_logger.setLevel(logging.DEBUG)

_formatter = logging.Formatter('|%(asctime)s| [%(levelname)s] (%(threadName)-10s) %(message)s')

_console_handler = logging.StreamHandler()
_console_handler.setLevel(logging.INFO)
_console_handler.setFormatter(_formatter)

_debug_rotating_file_handler = logging.handlers.RotatingFileHandler(_debug_logs_path + 'vols.publish.log',
                                                                    maxBytes=32 * (1024 ** 2), backupCount=64)
_debug_rotating_file_handler.setLevel(logging.DEBUG)
_debug_rotating_file_handler.setFormatter(_formatter)

_rotating_file_handler = logging.handlers.RotatingFileHandler(_logs_path + 'vols.publish.log',
                                                              maxBytes=32 * (1024 ** 2), backupCount=32)
_rotating_file_handler.setLevel(logging.INFO)
_rotating_file_handler.setFormatter(_formatter)

_logger.addHandler(_console_handler)
_logger.addHandler(_rotating_file_handler)
_logger.addHandler(_debug_rotating_file_handler)

_vol_publish_interval_in_seconds = 15 * 60


def main():

    try:

        ccy_pairs = myconfigs.read_currencypairs_config_into_list()
        vol_pub = VolsPublisher(ccy_pairs, VolSource.jpm, SpotSource.fenics,
                                KafkaBootstrapServer.wmc_cp_vm09, _vol_publish_interval_in_seconds,
                                include_extra_tenor_vols=True)

        vol_pub.start()

        while True:
            time.sleep(1.0)

    except KeyboardInterrupt:
        _logger.info('Keyboard interrupt.')
        vol_pub.stop()


if __name__ == '__main__':
    main()
