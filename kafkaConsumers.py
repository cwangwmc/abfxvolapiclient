from typing import *
from kafka import KafkaConsumer, TopicPartition
from enum import Enum

import logging
import json

_logger = logging.getLogger()

_kafka_bootstrap_servers_ct = 'wmc-ct-ws13-debian:9092'
_kafka_bootstrap_servers_uk = 'wmc-uk-ws01:9092'
_kafka_bootstrap_servers_cp = 'wmc-cp-vm09:9092'

_kafka_topic_vol_source = 'providers.vol.{source}'
_kafka_topic_vol_source_wildcard = 'providers.vol.{source}.*'
_kafka_topic_vol_source_ccypair = 'providers.vol.{source}.{ccypair}'

_kafka_topic_rfq_vols_source = 'providers.rfq.vols.{source}'
_kafka_topic_rfq_vols_source_wildcard = 'providers.rfq.vols.{source}.*'
_kafka_topic_rfq_vols_source_ccypair = 'providers.rfq.vols.{source}.{ccypair}'

_kafka_topic_rfq_tenordates = 'providers.rfq.tenordates'
_kafka_topic_rfq_tenordates_wildcard = 'providers.rfq.tenordates.*'
_kafka_topic_rfq_tenordates_ccypair = 'providers.rfq.tenordates.{ccypair}'

_kafka_topic_spot_source = 'providers.spots.{source}'


class VolSource(Enum):
    dbabfx = 1,
    citi = 2,
    jpm = 3


class SpotSource(Enum):
    fenics = 1


class KafkaBootstrapServer(Enum):
    wmc_ct_ws13 = 1,
    wmc_uk_ws01 = 2,
    wmc_cp_vm09 = 3


def _get_kafka_bootstrap_server_details(server: KafkaBootstrapServer) -> str:
    if (server == KafkaBootstrapServer.wmc_ct_ws13):
        return _kafka_bootstrap_servers_ct
    elif (server == KafkaBootstrapServer.wmc_uk_ws01):
        return _kafka_bootstrap_servers_uk
    elif (server == KafkaBootstrapServer.wmc_cp_vm09):
        return _kafka_bootstrap_servers_cp


class BaseKafkaConsumer():

    def __init__(self, server: KafkaBootstrapServer):
        self._bootstrap_server_details = _get_kafka_bootstrap_server_details(server)
        try:
            self._consumer = KafkaConsumer(bootstrap_servers=self._bootstrap_server_details)
        except Exception as exp:
            _logger.exception('Exception occurred while trying to establish Kafka Consumer at {} ...'
                              .format(self._bootstrap_server_details))
            _logger.exception(exp)

    def close(self):
        if self._consumer:
            self._consumer.close()


class SpotKafkaConsumer(BaseKafkaConsumer):

    def __init__(self, server: KafkaBootstrapServer, spot_source: SpotSource):
        BaseKafkaConsumer.__init__(self, server)
        self._spot_source = spot_source
        self._topic = _kafka_topic_spot_source.format(source=self._spot_source.name)
        self._tp = [TopicPartition(self._topic, 0)]
        self._consumer.assign(self._tp)

    @property
    def Source(self) -> SpotSource:
        return self._spot_source

    def get_last_spots(self) -> dict:
        self._consumer.seek_to_end()
        tp = self._tp[0]
        spots_dict = {}
        _logger.debug('Interrogating Topic {} Partition {} ...'.format(tp.topic, tp.partition))
        last_msg_offset = self._consumer.position(tp) - 1
        if last_msg_offset >= 0:
            _logger.debug('Topic {} Partition {} last offset {} ...'.format(tp.topic, tp.partition, last_msg_offset))
            self._consumer.seek(tp, last_msg_offset)
            tp_msg = next(self._consumer)
            spots_dict = json.loads(tp_msg.value, encoding='utf-8')
        else:
            _logger.warning('Topic {} Partition {} has last message offset {}'.format(tp.topic,
                                                                                      tp.partition, last_msg_offset))
        return spots_dict


class VolKafkaConsumer(BaseKafkaConsumer):

    def __init__(self, server: KafkaBootstrapServer, currency_pairs: List[str], vol_source: VolSource):
        BaseKafkaConsumer.__init__(self, server)
        self._currency_pairs = currency_pairs
        self._vol_source = vol_source
        self._topics = [_kafka_topic_vol_source_ccypair.format(source=self._vol_source.name, ccypair=ccypair)
                        for ccypair in self._currency_pairs]
        self._tps = [TopicPartition(t, 0) for t in self._topics]
        self._consumer.assign(self._tps)

    @property
    def Source(self) -> VolSource:
        return self._vol_source

    def get_last_vols(self) -> dict:
        self._consumer.seek_to_end()
        ccypair_surface_dict = {}
        for tp in self._tps:
            _logger.debug('Interrogating Topic {0} Partition {1} ...'.format(tp.topic, tp.partition))
            last_msg_offset = self._consumer.position(tp) - 1
            if last_msg_offset >= 0:
                _logger.debug('Topic {0} Partition {1} last offset {2} ...'.format(tp.topic, tp.partition, last_msg_offset))
                self._consumer.seek(tp, last_msg_offset)
                tp_msg = next(self._consumer)
                vol_surface_dict_from_tp_msg = json.loads(tp_msg.value, encoding='utf-8')
                ccypair = vol_surface_dict_from_tp_msg['Security']
                ccypair_surface_dict[ccypair] = vol_surface_dict_from_tp_msg
            else:
                _logger.warning('Topic {0} Partition {1} has last message offset {2}'.format(tp.topic,
                                                                                             tp.partition, last_msg_offset))

        return ccypair_surface_dict


class RfqVolKafkaConsumer(BaseKafkaConsumer):

    def __init__(self, server: KafkaBootstrapServer, currency_pairs: List[str], vol_source: VolSource):
        BaseKafkaConsumer.__init__(self, server)
        self._currency_pairs = currency_pairs
        self._vol_source = vol_source
        self._topics = [_kafka_topic_rfq_vols_source_ccypair.format(source=vol_source.name, ccypair=ccypair)
                        for ccypair in currency_pairs]
        self._tps = [TopicPartition(t, 0) for t in self._topics]
        self._consumer.assign(self._tps)

    @property
    def Source(self) -> VolSource:
        return self._vol_source

    def get_last_rfq_vols(self) -> dict:
        self._consumer.seek_to_end()
        ccypair_surface_dict = {}
        for tp in self._tps:
            _logger.debug('Interrogating Topic {0} Partition {1} ...'.format(tp.topic, tp.partition))
            last_msg_offset = self._consumer.position(tp) - 1
            if last_msg_offset >= 0:
                _logger.debug('Topic {0} Partition {1} last offset {2} ...'.format(tp.topic, tp.partition, last_msg_offset))
                self._consumer.seek(tp, last_msg_offset)
                tp_msg = next(self._consumer)
                vol_surface_dict_from_tp_msg = json.loads(tp_msg.value, encoding='utf-8')
                ccypair = vol_surface_dict_from_tp_msg['Security']
                ccypair_surface_dict[ccypair] = vol_surface_dict_from_tp_msg
            else:
                _logger.warning('Topic {0} Partition {1} has last message offset {2}'.format(tp.topic,
                                                                                             tp.partition, last_msg_offset))

        return ccypair_surface_dict


class TenorDatesKafkaConsumer(BaseKafkaConsumer):

    def __init__(self, server: KafkaBootstrapServer, currency_pairs: List[str]):
        BaseKafkaConsumer.__init__(self, server)
        self._currency_pairs = currency_pairs
        self._topics = [_kafka_topic_rfq_tenordates_ccypair.format(ccypair=ccypair)
                        for ccypair in self._currency_pairs]
        self._tps = [TopicPartition(t, 0) for t in self._topics]
        self._consumer.assign(self._tps)

    def get_last_tenordates(self) -> dict:
        self._consumer.seek_to_end()
        ccypair_rfq_tenordates_dict = {}
        for tp in self._tps:
            _logger.debug('Interrogating Topic {0} Partition {1} ...'.format(tp.topic, tp.partition))
            last_msg_offset = self._consumer.position(tp) - 1
            if last_msg_offset >= 0:
                _logger.debug('Topic {0} Partition {1} last offset {2} ...'.format(tp.topic, tp.partition, last_msg_offset))
                self._consumer.seek(tp, last_msg_offset)
                tp_msg = next(self._consumer)
                tenordates_dict_from_tp_msg = json.loads(tp_msg.value, encoding='utf-8')
                ccypair = tenordates_dict_from_tp_msg['CurrencyPair']
                ccypair_rfq_tenordates_dict[ccypair] = tenordates_dict_from_tp_msg
            else:
                _logger.warning('Topic {0} Partition {1} has last message offset {2}'.format(tp.topic,
                                                                                             tp.partition, last_msg_offset))

        return ccypair_rfq_tenordates_dict
