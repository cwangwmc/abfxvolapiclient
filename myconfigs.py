from typing import *

import os

alice_cert_filename = 'vol.requests.pem'
currency_pairs_config_filename = 'currencypairs.config'
abfx_vol_convention_config_filename = 'abfxVolConventions.config'


def current_file_dir_path() -> str:
    current_file_path = os.path.realpath(__file__)
    last_backslash_index = current_file_path.rfind('\\')
    current_file_dir_path = current_file_path[0: last_backslash_index + 1]
    return current_file_dir_path


def read_currencypairs_config_into_list() -> List[str]:
    dir_path = current_file_dir_path()
    currencypairs_config_file = open(dir_path + currency_pairs_config_filename, 'r')
    currencypairs_lines = currencypairs_config_file.readlines()
    currencypairs_config_file.close()
    # Getting rid of the trailing line breaks
    currencypairs_list = [l[:-1] for l in currencypairs_lines]
    return currencypairs_list
