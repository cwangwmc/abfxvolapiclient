from typing import *
from abfxVolClient import AbfxVolFetcher
from kafkaConsumers import SpotKafkaConsumer, SpotSource, KafkaBootstrapServer
from svcPyWin import BasePyWinService

import myconfigs
import logging
import logging.handlers
import time
import os


_current_file_dir_path = myconfigs.current_file_dir_path()

_debug_logs_dir = '_logs.svc.debug/'
_logs_dir = '_logs.svc/'

_debug_logs_path = _current_file_dir_path + _debug_logs_dir
_logs_path = _current_file_dir_path + _logs_dir

if not os.path.exists(_debug_logs_path):
    os.makedirs(_debug_logs_path)

if not os.path.exists(_logs_path):
    os.makedirs(_logs_path)

_logger = logging.getLogger()
_logger.setLevel(logging.DEBUG)

_formatter = logging.Formatter('|%(asctime)s| [%(levelname)s] (%(threadName)-10s) %(message)s')

_console_handler = logging.StreamHandler()
_console_handler.setLevel(logging.INFO)
_console_handler.setFormatter(_formatter)

_debug_rotating_file_handler = logging.handlers.RotatingFileHandler(
                                        _debug_logs_path + 'abfxvol.log',
                                        maxBytes=32 * (1024 ** 2), backupCount=64)

_debug_rotating_file_handler.setLevel(logging.DEBUG)
_debug_rotating_file_handler.setFormatter(_formatter)

_rotating_file_handler = logging.handlers.RotatingFileHandler(
                                        _logs_path + 'abfxvol.log',
                                        maxBytes=32 * (1024 ** 2), backupCount=32)

_rotating_file_handler.setLevel(logging.INFO)
_rotating_file_handler.setFormatter(_formatter)

_logger.addHandler(_console_handler)
_logger.addHandler(_debug_rotating_file_handler)
_logger.addHandler(_rotating_file_handler)

_abfx_username = 'cwang@whitemarigoldcapital.com'
_abfx_password = 'Shanghai83wmc#5'
_vol_kafka_producer_bootstrap_servers = 'wmc-cp-vm09:9092'
_max_ccypairs_per_volfetcher = 8
_vol_fetch_interval_in_seconds = 5 * 60
_re_auth_after_fetches = 48


class AbfxVolWinSvc(BasePyWinService):

    _svc_name_ = 'WMCDbAbfxLiveVolsService'
    _svc_display_name_ = 'WMC DB Abfx Live Vols Fetching Service'
    _svc_description_ = 'Fetch Live Fx Vols from Deutsche Autobahn Web Api and Transfer onto Kafka'

    _abfx_vol_fetchers = []

    def start(self):
        ccypairs = myconfigs.read_currencypairs_config_into_list()
        m = _max_ccypairs_per_volfetcher
        while ccypairs:
            spot_kfc = SpotKafkaConsumer(KafkaBootstrapServer.wmc_cp_vm09, SpotSource.fenics)
            if (len(ccypairs) <= m):
                vol_fetcher = AbfxVolFetcher(ccypairs, _vol_fetch_interval_in_seconds, _re_auth_after_fetches,
                                             _abfx_username, _abfx_password,
                                             _vol_kafka_producer_bootstrap_servers,
                                             spot_kfc)
                ccypairs = []
            else:
                vol_fetcher = AbfxVolFetcher(ccypairs[0:m], _vol_fetch_interval_in_seconds, _re_auth_after_fetches,
                                             _abfx_username, _abfx_password,
                                             _vol_kafka_producer_bootstrap_servers,
                                             spot_kfc)
                ccypairs = ccypairs[m:]
            self._abfx_vol_fetchers.append(vol_fetcher)
            vol_fetcher.start()
        self._is_running = True

    def stop(self):
        for vol_fetcher in self._abfx_vol_fetchers:
            vol_fetcher.stop()
        self._is_running = False


if __name__ == '__main__':
    AbfxVolWinSvc.parse_command_line()
