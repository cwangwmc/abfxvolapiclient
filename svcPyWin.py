import win32serviceutil
import win32service
import win32event
import servicemanager
import socket

import time


class BasePyWinService(win32serviceutil.ServiceFramework):

    _svc_name_ = 'PythonWinService'
    _svc_display_name_ = 'Python Windows Service'
    _svc_description_ = 'Python Windows Service Description'
    _is_running = False

    @classmethod
    def parse_command_line(cls):

        win32serviceutil.HandleCommandLine(cls)

    def __init__(self, args):

        win32serviceutil.ServiceFramework.__init__(self, args)
        self._wait_stop = win32event.CreateEvent(None, 0, 0, None)
        socket.setdefaulttimeout(60)

    # SvcStop
    # The method that will be called when the service is requested to stop
    def SvcStop(self):

        self.stop()
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self._wait_stop)

    # SvcDoRun
    # The method that will be called when the service is requested to start
    def SvcDoRun(self):

        self.start()
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_, ''))
        self.main()

    # start
    # The method that you will be asked to override if you need to do something
    # when the service is starting (before started)
    def start(self):

        pass

    # stop
    # The method that you will be asked to override if you need to do something
    # when the service is stopping (before stopped)
    def stop(self):

        pass

    # main
    # method that will contain the logic of your script,
    # usually in a loop that keeps it alive until the service is stopped
    def main(self):
        while self._is_running:
            time.sleep(1.0)
