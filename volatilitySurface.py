from typing import *
from enum import Enum

import datetime as dt
import json
import arrow


class Atm(Enum):
    Dn = 1
    Atmf = 2


class DeltaOrientation(Enum):
    Call = 1
    Put = 2


class DeltaType(Enum):
    SpotBp = 1
    SpotPc = 2
    ForwardBp = 3
    ForwardPc = 4


Delta = Tuple[int, DeltaOrientation, DeltaType]
VolSmilePointLocation = Union[Atm, Delta]


class Tenor:

    _days_in_a_week = 7
    _days_in_a_month = 30
    _days_in_a_year = 360

    def __init__(self, tenor_code: str) -> None:
        self._tenor_code = tenor_code.upper()
        if (self._tenor_code == 'ON'):
            self._num = 1
            self._period = 'D'
        else:
            self._num = int(self._tenor_code[:-1])
            self._period = self._tenor_code[-1]

    @property
    def TenorCode(self):
        return self._tenor_code

    def _resolve_to_indicative_days(self) -> int:
        period_days = 0
        if self._period == 'D':
            period_days = 1
        elif self._period == 'W':
            period_days = Tenor._days_in_a_week
        elif self._period == 'M':
            period_days = Tenor._days_in_a_month
        elif self._period == 'Y':
            period_days = Tenor._days_in_a_year
        return self._num * period_days

    def __eq__(self, other) -> bool:
        return self._resolve_to_indicative_days() == other._resolve_to_indicative_days()

    def __ne__(self, other) -> bool:
        return self._resolve_to_indicative_days() != other._resolve_to_indicative_days()

    def __gt__(self, other) -> bool:
        return self._resolve_to_indicative_days() > other._resolve_to_indicative_days()

    def __ge__(self, other) -> bool:
        return self._resolve_to_indicative_days() >= other._resolve_to_indicative_days()

    def __le__(self, other) -> bool:
        return self._resolve_to_indicative_days() <= other._resolve_to_indicative_days()

    def __lt__(self, other) -> bool:
        return self._resolve_to_indicative_days() < other._resolve_to_indicative_days()

    def __str__(self) -> str:
        return self._tenor_code

    def __repr__(self) -> str:
        return self.__str__()


class VolatilitySmilePoint:

    def __init__(self, volInPercentage: float, volSmilePtLoc: VolSmilePointLocation) -> None:
        self._vol = volInPercentage
        self._is_atm_vol_point = False
        self._atmType = None
        self._delta_details = None
        if isinstance(volSmilePtLoc, Atm):
            self._is_atm_vol_point = True
            self._atmType = volSmilePtLoc
        else:
            self._delta_details = volSmilePtLoc

    @classmethod
    def from_dict(cls, dict_value: Dict) -> 'VolatilitySmilePoint':
        if dict_value['IsAtm']:
            vol_value = dict_value['Vol']
            atm_type_value = dict_value['AtmType']

            atmType = None
            if atm_type_value == Atm.Dn.name:
                atmType = Atm.Dn
            elif atm_type_value == Atm.Atmf.name:
                atmType = Atm.Atmf

            return cls(vol_value, atmType)

        else:
            vol_value = dict_value['Vol']
            delta_details_dict = dict_value['DeltaDetails']
            delta = delta_details_dict['Delta']
            delta_orientation_value = delta_details_dict['DeltaOrientation']
            delta_type_value = delta_details_dict['DeltaType']

            delta_orientation = None
            if delta_orientation_value == DeltaOrientation.Call.name:
                delta_orientation = DeltaOrientation.Call
            elif delta_orientation_value == DeltaOrientation.Put.name:
                delta_orientation = DeltaOrientation.Put

            delta_type = None
            if delta_type_value == DeltaType.SpotBp.name:
                delta_type = DeltaType.SpotBp
            elif delta_type_value == DeltaType.SpotPc.name:
                delta_type = DeltaType.SpotPc
            elif delta_type_value == DeltaType.ForwardBp.name:
                delta_type = DeltaType.ForwardBp
            elif delta_type_value == DeltaType.ForwardPc.name:
                delta_type = DeltaType.ForwardPc

            return cls(vol_value, (delta, delta_orientation, delta_type))

    @property
    def VolInDecimal(self) -> float:
        return self._vol / 100

    @property
    def VolInPercentage(self) -> float:
        return self._vol

    @property
    def IsAtm(self) -> bool:
        return self._is_atm_vol_point

    @property
    def AtmType(self) -> Atm:
        return self._atmType

    @property
    def DeltaDetails(self) -> Delta:
        return self._delta_details

    @property
    def DeltaAsDecimal(self) -> float:
        if not self.IsAtm:
            return list(self._delta_details)[0] / 100

    @property
    def DeltaAsInt(self) -> int:
        if not self.IsAtm:
            return list(self._delta_details)[0]
        else:
            return None

    @property
    def DeltaForCallOrPut(self) -> DeltaOrientation:
        if not self.IsAtm:
            return list(self._delta_details)[1]
        else:
            return None

    @property
    def DeltaType(self) -> DeltaType:
        if not self.IsAtm:
            return list(self._delta_details)[2]
        else:
            return None

    def to_dict(self) -> Dict:
        dict_repr = {}
        if self.IsAtm:
            dict_repr['Vol'] = self.VolInPercentage
            dict_repr['IsAtm'] = True
            dict_repr['AtmType'] = self.AtmType.name
            dict_repr['DeltaDetails'] = None
        else:
            dict_repr['Vol'] = self.VolInPercentage
            dict_repr['IsAtm'] = False
            dict_repr['AtmType'] = None
            dict_repr['DeltaDetails'] = {'Delta': self.DeltaAsInt,
                                         'DeltaOrientation': self.DeltaForCallOrPut.name,
                                         'DeltaType': self.DeltaType.name
                                         }
        return dict_repr

    def __str__(self) -> str:
        str_repr = ''
        if (self.IsAtm):
            str_repr = f'{self.AtmType.name} {self.VolInPercentage}'
        else:
            str_repr = f'{self.DeltaAsInt}D-{self.DeltaForCallOrPut.name}-{self.DeltaType.name} {self.VolInPercentage}'
        return str_repr

    def __repr__(self) -> str:
        return self.__str__()


class VolatilitySmile:

    def __init__(self, tenor: str, volSmilePoints: List[VolatilitySmilePoint]) -> None:
        self._tenor = tenor.upper()
        self._vol_smile_points = volSmilePoints
        self._atm_vol_point = [vpt for vpt in volSmilePoints if vpt.IsAtm][0]

    @classmethod
    def from_dict(cls, dict_value: dict) -> 'VolatilitySmile':
        tenor_value = dict_value['Tenor']
        vol_smile_points_dicts = dict_value['VolSmilePoints']

        return cls(tenor_value, [VolatilitySmilePoint.from_dict(dct) for dct in vol_smile_points_dicts])

    @property
    def Tenor(self) -> str:
        return self._tenor

    @property
    def VolSmilePoints(self) -> List[VolatilitySmilePoint]:
        return self._vol_smile_points

    @property
    def AtmVolPoint(self) -> VolatilitySmilePoint:
        return self._atm_vol_point

    @property
    def DeltaVolPoints(self) -> List[VolatilitySmilePoint]:
        return [vol_pt for vol_pt in self._vol_smile_points if not vol_pt.IsAtm]

    def vol_smile_points_deltas_as_int(self) -> List[int]:
        deltas_in_int = list(set([vol_pt.DeltaAsInt for vol_pt in self.DeltaVolPoints]))
        deltas_in_int.sort(reverse=True)
        return deltas_in_int

    def vol_smile_points_deltas_as_decimal(self) -> List[float]:
        deltas_in_decimals = list(set([vol_pt.DeltaAsDecimal for vol_pt in self.DeltaVolPoints]))
        deltas_in_decimals.sort(reverse=True)
        return deltas_in_decimals

    def vol_smile_points_for_int_delta(self,
                                       delta_in_int: int) -> List[VolatilitySmilePoint]:
        return [vol_pt for vol_pt in self._vol_smile_points if vol_pt.DeltaAsInt == delta_in_int]

    def vol_smile_points_for_decimal_delta(self,
                                           delta_in_decimal: float) -> List[VolatilitySmilePoint]:
        return [vol_pt for vol_pt in self._vol_smile_points if vol_pt.DeltaAsDecimal == delta_in_decimal]

    def vol_smile_points_for_delta_orientation(self,
                                               delta_orientation: DeltaOrientation) -> List[VolatilitySmilePoint]:
        return [vol_pt for vol_pt in self._vol_smile_points if vol_pt.DeltaForCallOrPut == delta_orientation]

    def to_dict(self) -> Dict:
        dict_repr = {}
        dict_repr['Tenor'] = self.Tenor
        dict_repr['VolSmilePoints'] = [vol_smile_point.to_dict() for vol_smile_point in self.VolSmilePoints]
        return dict_repr

    def __str__(self) -> str:
        str_repr = '{0} ATM: {1}\t'.format(self.Tenor, self.AtmVolPoint.VolInPercentage)
        for d in self.vol_smile_points_deltas_as_int():
            vol_pts_d = self.vol_smile_points_for_int_delta(d)
            for vol_pt_d in vol_pts_d:
                str_repr += '{0}D {1}: {2}\t'.format(d, vol_pt_d.DeltaForCallOrPut.name, vol_pt_d.VolInPercentage)

        return str_repr

    def __repr__(self) -> str:
        return self.__str__()


class VolatilitySurface:

    def __init__(self, securitySymbol: str, asOfTimestamp: dt.datetime, volSmiles: List[VolatilitySmile],
                 source: str = 'Unknown', reference_spot: float = None, reference_date: dt.datetime = None) -> None:
        self._securitySymbol = securitySymbol.upper()
        self._asof_timestamp = asOfTimestamp
        self._source = source
        self._reference_spot = reference_spot
        if self._reference_spot:
            self._reference_spot = round(self._reference_spot, 4)
        self._reference_date = reference_date
        self._vol_smile_tenors = self._sort_tenors([vs.Tenor for vs in volSmiles])
        self._vol_smiles = []
        for tnr in self._vol_smile_tenors:
            self._vol_smiles.append([vs for vs in volSmiles if vs.Tenor == tnr][0])

    @classmethod
    def from_json(cls, json_value: str) -> 'VolatilitySurface':
        dict_value = json.loads(json_value)

        return cls.from_dict(dict_value)

    @classmethod
    def from_dict(cls, dict_value: dict) -> 'VolatilitySurface':
        security_sym = dict_value['Security']
        date_time = arrow.get(dict_value['AsOf']).datetime
        source = dict_value['Source']
        ref_spot = dict_value['ReferenceSpot']
        if ref_spot:
            ref_spot = float(ref_spot)
        ref_date = dict_value['ReferenceDate']
        if ref_date:
            ref_date = arrow.get(ref_date).datetime
        vol_smiles = None
        dict_value_vol_smiles = dict_value['VolSmiles']
        if (type(dict_value_vol_smiles) is list):
            vol_smiles = [VolatilitySmile.from_dict(vol_smile_dict) for vol_smile_dict in dict_value_vol_smiles]
        elif (type(dict_value_vol_smiles) is dict):
            vol_smiles = [VolatilitySmile.from_dict(vol_smile_dict) for vol_smile_dict in dict_value_vol_smiles.values()]

        return cls(security_sym, date_time, vol_smiles, source, ref_spot, ref_date)

    @property
    def SecuritySymbol(self) -> str:
        return self._securitySymbol

    @property
    def AsOfTimeStamp(self) -> dt.datetime:
        return self._asof_timestamp

    @property
    def Source(self) -> str:
        return self._source

    @property
    def ReferenceSpot(self) -> float:
        return self._reference_spot

    @ReferenceSpot.setter
    def ReferenceSpot(self, value) -> None:
        self._reference_spot = value

    @property
    def ReferenceDate(self) -> dt.datetime:
        return self._reference_date

    @ReferenceDate.setter
    def ReferenceDate(self, value) -> None:
        self._reference_date = value

    @property
    def ReferenceDateIso(self) -> str:
        return arrow.get(self._reference_date).isoformat()

    @property
    def VolSmiles(self) -> List[VolatilitySmile]:
        return self._vol_smiles

    @property
    def VolSmileTenors(self) -> List[str]:
        return self._vol_smile_tenors

    def _sort_tenors(cls, tenors: List[str]) -> List[str]:
        tenors_objs = [Tenor(t) for t in tenors]
        sorted_tenors_objs = sorted(tenors_objs)
        sorted_tenors_strs = [t.TenorCode for t in sorted_tenors_objs]
        return sorted_tenors_strs

    def try_add_vol_smiles(self, vol_smiles: List[VolatilitySmile]) -> bool:
        tenors_to_add = [vol_smile.Tenor for vol_smile in vol_smiles]
        tenors_to_add_set = set(tenors_to_add)
        tenors_set = set(self._vol_smile_tenors)

        if (tenors_to_add_set & tenors_set):
            return False
        else:
            sorted_combo_vol_smile_tenors = self._sort_tenors(self._vol_smile_tenors + tenors_to_add)
            combo_vol_smiles = self._vol_smiles + vol_smiles

            sorted_combo_vol_smiles = []
            for tnr in sorted_combo_vol_smile_tenors:
                tnr_vol_smile = [vol_smile for vol_smile in combo_vol_smiles if vol_smile.Tenor == tnr][0]
                sorted_combo_vol_smiles.append(tnr_vol_smile)

            self._vol_smile_tenors = sorted_combo_vol_smile_tenors
            self._vol_smiles = sorted_combo_vol_smiles

            return True

    def vol_smile_for_tenor(self, tenor: str) -> Union[VolatilitySmile, None]:
        vol_smile_candidate_list = [vol_smile for vol_smile in self.VolSmiles if vol_smile.Tenor == tenor.upper()]
        if len(vol_smile_candidate_list) > 0:
            return vol_smile_candidate_list[0]

    def vol_smile_points_atm(self, tenors: List[str] = None) -> List[VolatilitySmilePoint]:
        vol_smile_atm_points = []
        if tenors:
            sorted_tenors = self._sort_tenors(tenors)
            vol_smile_atm_points = [vol_smile.AtmVolPoint for vol_smile in self.VolSmiles if vol_smile.Tenor in sorted_tenors]
        else:
            vol_smile_atm_points = [vol_smile.AtmVolPoint for vol_smile in self.VolSmiles]

        return vol_smile_atm_points

    def vol_smile_points_for_delta(self, delta: int, delta_call_put: DeltaOrientation,
                                   tenors: List[str] = None) -> List[VolatilitySmilePoint]:
        vol_smile_delta_points = []
        flat_vol_pts = []
        if tenors:
            sorted_tenors = self._sort_tenors(tenors)
            flat_vol_pts = [vol_pt for vol_smile in self.VolSmiles for vol_pt in vol_smile.VolSmilePoints
                            if vol_smile.Tenor in sorted_tenors]
        else:
            flat_vol_pts = [vol_pt for vol_smile in self.VolSmiles for vol_pt in vol_smile.VolSmilePoints]

        vol_smile_delta_points = [vol_pt for vol_pt in flat_vol_pts if not vol_pt.IsAtm and vol_pt.DeltaAsInt == delta
                                  and vol_pt.DeltaForCallOrPut == delta_call_put]

        return vol_smile_delta_points

    def to_dict(self) -> Dict:
        dict_repr = {}
        dict_repr['Security'] = self.SecuritySymbol
        dict_repr['AsOf'] = arrow.get(self.AsOfTimeStamp).isoformat()
        dict_repr['Source'] = self.Source
        dict_repr['ReferenceSpot'] = self.ReferenceSpot
        dict_repr['ReferenceDate'] = None
        if self.ReferenceDate:
            dict_repr['ReferenceDate'] = arrow.get(self.ReferenceDate).isoformat()
        dict_repr['VolSmiles'] = [vol_smile.to_dict() for vol_smile in self.VolSmiles]
        return dict_repr

    def to_json(self) -> str:
        dict_repr = self.to_dict()
        json_repr = json.dumps(dict_repr)
        return json_repr

    def __str__(self):
        str_repr = '{ccypair}, Ref Spot: {refspot}, Ref Date: {refdate}, As Of: {asof}\n'.format(
            ccypair=self.SecuritySymbol,
            refspot=self.ReferenceSpot, refdate=self.ReferenceDateIso,
            asof=self.AsOfTimeStamp)
        for tnr in self.VolSmileTenors:
            tnr_vol_smile = self.vol_smile_for_tenor(tnr)
            str_repr += str(tnr_vol_smile) + '\n'
        return str_repr

    def __repr__(self):
        return self.__str__()
