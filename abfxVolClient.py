from typing import *
from requests import Session, Request
from requests.packages.urllib3 import exceptions
from kafka import KafkaProducer
from kafkaConsumers import SpotKafkaConsumer

import requests as reqs
import xml.etree.ElementTree as et
import abfxVolResponseXmlParser as abfxVolParser
import datetime as dt
import time
import logging
import threading
import arrow
import json

reqs.packages.urllib3.disable_warnings(exceptions.InsecureRequestWarning)

_logger = logging.getLogger()

_db_abfx_vol_api_end_point = 'https://app-uk.autobahn.db.com/imd-web/rest/fxVols/getVolatilities?quote={ccypair}'

_dbabfx_vol_topic = 'providers.vol.dbabfx'
_dbabfx_vol_per_ccypair_topic = 'providers.vol.dbabfx.{ccypair}'


def _initiiate_session_and_get_logon_url() -> Tuple[Session, str]:

    eurusd_req_url = _db_abfx_vol_api_end_point.format(ccypair='EURUSD')
    new_session = Session()
    logon_request = new_session.get(eurusd_req_url, verify=False)

    return (new_session, logon_request.url)


def _authenticate_with_valid_credentials(session: Session, logon_url: str,
                                         abfx_username: str, abfx_password: str) -> bool:

    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    auth_data = {
                 'CTAuthMode': 'BASIC',
                 'auth_mode': 'BASIC',
                 'sso_operation': 'authenticate_user',
                 'user': abfx_username,
                 'password': abfx_password,
                 'rememberuseridvalue': 'false'
                 }

    auth_response = session.post(logon_url, data=auth_data, headers=headers, verify=False)
    is_auth_successful = auth_response.status_code == 200 and len(auth_response.history) > 0

    return is_auth_successful


class AbfxVolFetcher(threading.Thread):

    _session = None
    _logon_url = ''
    _logon_retries = 3
    _fetches = 0

    def __init__(self, ccy_pairs: List[str], fetch_interval_in_seconds: int, re_init_auth_after_fetches: int,
                 abfx_username: str, abfx_password: str,
                 vol_kfp_bootstrap_servers: str, spot_kfc: SpotKafkaConsumer,
                 daily_start_time: dt.time = None, daily_end_time: dt.time = None):
        threading.Thread.__init__(self)
        self._fetch_interval_in_seconds = fetch_interval_in_seconds
        self._re_init_auth_after_fetches = re_init_auth_after_fetches
        self._abfx_username = abfx_username
        self._abfx_password = abfx_password
        self._ccy_pairs = ccy_pairs
        self._stop_event = threading.Event()
        self._vol_kfp_bootstrap_servers = vol_kfp_bootstrap_servers
        self._vol_kfp = KafkaProducer(bootstrap_servers=self._vol_kfp_bootstrap_servers)
        self._spot_kfc = spot_kfc
        self._daily_start_time = daily_start_time
        self._daily_end_time = daily_end_time

    def run(self):
        if (self._init_auth()):
            self._fetch_send_vols()
            self._fetches = 1
            while not self._stop_event.wait(self._fetch_interval_in_seconds):
                if (self._fetches >= self._re_init_auth_after_fetches):
                    _logger.info('Re-initialise and authenticate session after {} fetches...'.format(self._fetches))
                    if (self._init_auth()):
                        _logger.info('Succeeded re-initialising and authenticating session.')
                        _logger.info('Resetting fetches count...')
                        self._fetches = 0
                    else:
                        _logger.fatal('Failed to re-initialise and authenticate session. Exiting...')
                        self.tidy_up()
                        return
                self._fetch_send_vols()
                self._fetches += 1
        else:
            _logger.fatal('Failed to initialise and authenticate session. Exiting...')
            self.tidy_up()

    def stop(self):
        self._stop_event.set()

    def tidy_up(self):
        if self._vol_kfp:
            self._vol_kfp.close()
            self._vol_kfp = None
        if self._spot_kfc:
            self._spot_kfc.close()
            self._spot_kfc = None

    def _init_auth(self) -> bool:
        session, logon_url = _initiiate_session_and_get_logon_url()
        if (_authenticate_with_valid_credentials(session, logon_url, self._abfx_username, self._abfx_password)):
            self._session = session
            self._logon_url = logon_url
            return True
        else:
            _logger.exception('Failed to authenticate with credentials ... username: {}, password: {}'
                              .format(self._abfx_username, self._abfx_password))
            return False

    def _fetch_send_vols(self):

        ccypairs_ref_spots = self._fetch_ref_spots()

        for ccy_pair in self._ccy_pairs:

            _logger.info('Starting vol fetch for {0} ...'.format(ccy_pair))

            db_abfx_vol_resp_xml = ''
            try:
                db_abfx_vol_resp = self._session.get(_db_abfx_vol_api_end_point.format(ccypair=ccy_pair, verify=False))
                db_abfx_vol_resp_xml = db_abfx_vol_resp.text
            except Exception as exp:
                _logger.exception('Exception occurred while fetching vol for {}.'.format(ccy_pair))
                _logger.debug('Exception details:\n{}'.format(exp))

            _logger.info('Finished vol fetch for {0}.'.format(ccy_pair))

            if (db_abfx_vol_resp_xml and (ccy_pair in ccypairs_ref_spots)):
                try:
                    db_abfx_vol_surf = abfxVolParser.abfx_vol_surface_from_xml(db_abfx_vol_resp_xml)
                except ValueError as err:
                    _logger.error('ValueError found in abfx vol response xml ...\n{0}'.format(err))
                    _logger.debug('Response xml:\n{0}'.format(db_abfx_vol_resp_xml))

                if db_abfx_vol_surf:

                    db_abfx_vol_surf.ReferenceSpot = ccypairs_ref_spots[ccy_pair]
                    db_abfx_vol_surf_json = db_abfx_vol_surf.to_json()

                    _logger.info('Vol Surface {0}, Ref Spot {1}, As of {2}'.format(ccy_pair, db_abfx_vol_surf.ReferenceSpot,
                                                                                   db_abfx_vol_surf.AsOfTimeStamp))

                    if (self._vol_kfp):

                        dbabfx_vol_topic_per_ccypair = _dbabfx_vol_per_ccypair_topic.format(ccypair=ccy_pair)

                        _logger.info('Preparing to send vol surface to Kafka Server: {} - Kafka Topic: {}'
                                     .format(self._vol_kfp_bootstrap_servers, dbabfx_vol_topic_per_ccypair))

                        self._vol_kfp.send(_dbabfx_vol_topic,
                                           db_abfx_vol_surf_json.encode(encoding='utf_8'))
                        self._vol_kfp.send(dbabfx_vol_topic_per_ccypair,
                                           db_abfx_vol_surf_json.encode(encoding='utf_8'))
                        self._vol_kfp.flush()

                        _logger.info('Vol Surface of currency pair {} has been sent.'.format(ccy_pair))

    def _fetch_ref_spots(self) -> Dict:

        ccypairs_ref_spots = {}

        try:

            spots_dict = self._spot_kfc.get_last_spots()

            ccypairs_spots = [spot_quote for spot_quote in spots_dict['quotes']
                              if spot_quote['ccy_pair'] in self._ccy_pairs]

            for ccypair_spot in ccypairs_spots:
                ccypairs_ref_spots[ccypair_spot['ccy_pair']] = ccypair_spot['mid']

        except Exception as exp:
            _logger.exception('Exception occurred while trying to fetch Spots...')
            _logger.debug('Exception details:\n{}'.format(exp))

        return ccypairs_ref_spots
