from volatilitySurface import *

import unittest
import json


class TestVolatilitySurface(unittest.TestCase):

    def setUp(self):
        _atmDnVolPt = VolatilitySmilePoint(5.61, Atm.Dn)
        _25cVolPt = VolatilitySmilePoint(5.93, (25, DeltaOrientation.Call, DeltaType.SpotBp))
        _25pVolPt = VolatilitySmilePoint(5.93, (25, DeltaOrientation.Put, DeltaType.SpotBp))
        _10cVolPt = VolatilitySmilePoint(6.17, (10, DeltaOrientation.Call, DeltaType.SpotBp))
        _10pVolPt = VolatilitySmilePoint(6.16, (10, DeltaOrientation.Put, DeltaType.SpotBp))

        self._vol_smile_points_list_a = [_10pVolPt, _25pVolPt, _atmDnVolPt, _25cVolPt, _10cVolPt]

        _atmfVolPt = VolatilitySmilePoint(7.30, Atm.Atmf)
        _25cVolPt = VolatilitySmilePoint(7.84, (25, DeltaOrientation.Call, DeltaType.ForwardBp))
        _25pVolPt = VolatilitySmilePoint(7.38, (25, DeltaOrientation.Put, DeltaType.ForwardBp))
        _10cVolPt = VolatilitySmilePoint(8.79, (10, DeltaOrientation.Call, DeltaType.ForwardBp))
        _10pVolPt = VolatilitySmilePoint(8.40, (10, DeltaOrientation.Put, DeltaType.ForwardBp))

        self._vol_smile_points_list_b = [_10pVolPt, _25pVolPt, _atmfVolPt, _25cVolPt, _10cVolPt]

        _atmfVolPt = VolatilitySmilePoint(8.19, Atm.Atmf)
        _25cVolPt = VolatilitySmilePoint(7.88, (25, DeltaOrientation.Call, DeltaType.ForwardBp))
        _25pVolPt = VolatilitySmilePoint(9.48, (25, DeltaOrientation.Put, DeltaType.ForwardBp))
        _10cVolPt = VolatilitySmilePoint(7.95, (10, DeltaOrientation.Call, DeltaType.ForwardBp))
        _10pVolPt = VolatilitySmilePoint(11.32, (10, DeltaOrientation.Put, DeltaType.ForwardBp))

        self._vol_smile_points_list_c = [_10pVolPt, _25pVolPt, _atmfVolPt, _25cVolPt, _10cVolPt]

    def test_atm_vol_point_construction(self):
        atmDnVolPt = VolatilitySmilePoint(8.23, Atm.Dn)

        self.assertEqual(atmDnVolPt.VolInDecimal, 0.0823)
        self.assertEqual(atmDnVolPt.VolInPercentage, 8.23)
        self.assertEqual(atmDnVolPt.IsAtm, True)
        self.assertEqual(atmDnVolPt.AtmType, Atm.Dn)
        self.assertIsNone(atmDnVolPt.DeltaDetails)

        atmfVolPt = VolatilitySmilePoint(12.58, Atm.Atmf)

        self.assertEqual(atmfVolPt.VolInDecimal, 0.1258)
        self.assertEqual(atmfVolPt.VolInPercentage, 12.58)
        self.assertEqual(atmfVolPt.IsAtm, True)
        self.assertEqual(atmfVolPt.AtmType, Atm.Atmf)
        self.assertIsNone(atmfVolPt.DeltaDetails)

    def test_atm_vol_point_dict_codec(self):
        atmDnVolPt = VolatilitySmilePoint(8.23, Atm.Dn)
        atmfVolPt = VolatilitySmilePoint(12.58, Atm.Atmf)

        atmDnVolPt_to_dict = atmDnVolPt.to_dict()
        atmfVolPt_to_dict = atmfVolPt.to_dict()

        self.assertIsNotNone(atmDnVolPt_to_dict)
        self.assertIsNotNone(atmfVolPt_to_dict)

        atmDnVolPt_from_dict = VolatilitySmilePoint.from_dict(atmDnVolPt_to_dict)
        atmfVolPt_from_dict = VolatilitySmilePoint.from_dict(atmfVolPt_to_dict)

        self.assertIsNotNone(atmDnVolPt_from_dict)
        self.assertIsNotNone(atmfVolPt_from_dict)

        self.assertEqual(atmDnVolPt_from_dict.VolInPercentage, 8.23)
        self.assertEqual(atmDnVolPt_from_dict.IsAtm, True)
        self.assertEqual(atmDnVolPt_from_dict.AtmType, Atm.Dn)
        self.assertIsNone(atmDnVolPt_from_dict.DeltaDetails)

        self.assertEqual(atmfVolPt_from_dict.VolInPercentage, 12.58)
        self.assertEqual(atmfVolPt_from_dict.IsAtm, True)
        self.assertEqual(atmfVolPt_from_dict.AtmType, Atm.Atmf)
        self.assertIsNone(atmfVolPt_from_dict.DeltaDetails)

    def test_atm_vol_point_json_codec(self):
        atmDnVolPt = VolatilitySmilePoint(8.23, Atm.Dn)
        atmDnVolPt_dict = atmDnVolPt.to_dict()
        atmDnVolPt_json_obj = json.dumps(atmDnVolPt_dict)

        self.assertIsNotNone(atmDnVolPt_json_obj)

        atmDnVolPt_dict = json.loads(atmDnVolPt_json_obj)

        self.assertIsNotNone(atmDnVolPt_dict)

    def test_25_10_delta_vol_point_construction(self):
        _25cVolPt = VolatilitySmilePoint(3.21, (25, DeltaOrientation.Call, DeltaType.SpotPc))

        self.assertEqual(_25cVolPt.IsAtm, False)
        self.assertIsNone(_25cVolPt.AtmType)
        self.assertEqual(_25cVolPt.VolInDecimal, 0.0321)
        self.assertEqual(_25cVolPt.VolInPercentage, 3.21)
        self.assertIsNotNone(_25cVolPt.DeltaDetails)
        self.assertEqual(_25cVolPt.DeltaAsDecimal, 0.25)
        self.assertEqual(_25cVolPt.DeltaAsInt, 25)

        _10pVolPt = VolatilitySmilePoint(2.78, (10, DeltaOrientation.Put, DeltaType.ForwardBp))

        self.assertEqual(_10pVolPt.IsAtm, False)
        self.assertEqual(_10pVolPt.VolInDecimal, 0.0278)
        self.assertEqual(_10pVolPt.VolInPercentage, 2.78)
        self.assertIsNotNone(_10pVolPt.DeltaDetails)
        self.assertEqual(_10pVolPt.DeltaAsDecimal, 0.10)
        self.assertEqual(_10pVolPt.DeltaAsInt, 10)
        self.assertEqual(_10pVolPt.DeltaForCallOrPut, DeltaOrientation.Put)
        self.assertEqual(_10pVolPt.DeltaType, DeltaType.ForwardBp)

    def test_25_10_delta_vol_point_dict_codec(self):
        sb25c = VolatilitySmilePoint(3.21, (25, DeltaOrientation.Call, DeltaType.SpotBp))
        sp25p = VolatilitySmilePoint(1.21, (25, DeltaOrientation.Put, DeltaType.SpotPc))
        fb10c = VolatilitySmilePoint(5.21, (10, DeltaOrientation.Call, DeltaType.ForwardBp))
        fp10p = VolatilitySmilePoint(2.21, (10, DeltaOrientation.Put, DeltaType.ForwardPc))

        sb25c_to_dict = sb25c.to_dict()
        sp25p_to_dict = sp25p.to_dict()
        fb10c_to_dict = fb10c.to_dict()
        fp10p_to_dict = fp10p.to_dict()

        sb25c_from_dict = VolatilitySmilePoint.from_dict(sb25c_to_dict)
        sp25p_from_dict = VolatilitySmilePoint.from_dict(sp25p_to_dict)
        fb10c_from_dict = VolatilitySmilePoint.from_dict(fb10c_to_dict)
        fp10p_from_dict = VolatilitySmilePoint.from_dict(fp10p_to_dict)

        self.assertIsNotNone(sb25c_from_dict)
        self.assertEqual(sb25c_from_dict.VolInPercentage, 3.21)
        self.assertEqual(sb25c_from_dict.IsAtm, False)
        self.assertEqual(sb25c_from_dict.DeltaAsInt, 25)
        self.assertEqual(sb25c_from_dict.DeltaForCallOrPut, DeltaOrientation.Call)
        self.assertEqual(sb25c_from_dict.DeltaType, DeltaType.SpotBp)

        self.assertIsNotNone(sp25p_from_dict)
        self.assertEqual(sp25p_from_dict.VolInPercentage, 1.21)
        self.assertEqual(sp25p_from_dict.IsAtm, False)
        self.assertEqual(sp25p_from_dict.DeltaAsInt, 25)
        self.assertEqual(sp25p_from_dict.DeltaForCallOrPut, DeltaOrientation.Put)
        self.assertEqual(sp25p_from_dict.DeltaType, DeltaType.SpotPc)

        self.assertIsNotNone(fb10c_from_dict)
        self.assertEqual(fb10c_from_dict.VolInPercentage, 5.21)
        self.assertEqual(fb10c_from_dict.IsAtm, False)
        self.assertEqual(fb10c_from_dict.DeltaAsInt, 10)
        self.assertEqual(fb10c_from_dict.DeltaForCallOrPut, DeltaOrientation.Call)
        self.assertEqual(fb10c_from_dict.DeltaType, DeltaType.ForwardBp)

        self.assertIsNotNone(fp10p_from_dict)
        self.assertEqual(fp10p_from_dict.VolInPercentage, 2.21)
        self.assertEqual(fp10p_from_dict.IsAtm, False)
        self.assertEqual(fp10p_from_dict.DeltaAsInt, 10)
        self.assertEqual(fp10p_from_dict.DeltaForCallOrPut, DeltaOrientation.Put)
        self.assertEqual(fp10p_from_dict.DeltaType, DeltaType.ForwardPc)

    def test_25_10_delta_vol_point_json_codec(self):
        _25cVolPt = VolatilitySmilePoint(3.21, (25, DeltaOrientation.Call, DeltaType.SpotPc))
        _10pVolPt = VolatilitySmilePoint(2.78, (10, DeltaOrientation.Put, DeltaType.ForwardBp))

        _25cVolPt_dict = _25cVolPt.to_dict()
        _10pVolPt_dict = _10pVolPt.to_dict()

        _25cVolPt_json_obj = json.dumps(_25cVolPt_dict)
        _10pVolPt_json_obj = json.dumps(_10pVolPt_dict)

        self.assertIsNotNone(_25cVolPt_json_obj)
        self.assertIsNotNone(_10pVolPt_json_obj)

        _25cVolPt_dict_decoded = json.loads(_25cVolPt_json_obj)
        _10pVolPt_dict_decoded = json.loads(_10pVolPt_json_obj)

        self.assertIsNotNone(_25cVolPt_dict_decoded)
        self.assertIsNotNone(_10pVolPt_dict_decoded)

        self.assertEqual(_25cVolPt_dict, _25cVolPt_dict_decoded)
        self.assertEqual(_10pVolPt_dict, _10pVolPt_dict_decoded)

    def test_vol_smile_construction(self):
        _atmDnVolPt = VolatilitySmilePoint(8.23, Atm.Dn)
        _25cVolPt = VolatilitySmilePoint(3.24, (25, DeltaOrientation.Call, DeltaType.SpotBp))
        _10cVolPt = VolatilitySmilePoint(2.12, (10, DeltaOrientation.Call, DeltaType.SpotBp))
        _25pVolPt = VolatilitySmilePoint(6.78, (25, DeltaOrientation.Put, DeltaType.SpotBp))
        _10pVolPt = VolatilitySmilePoint(7.56, (10, DeltaOrientation.Put, DeltaType.SpotBp))

        volSmile = VolatilitySmile(tenor='1M',
                                   volSmilePoints=[_10pVolPt, _25pVolPt, _atmDnVolPt, _25cVolPt, _10cVolPt])

        self.assertIsNotNone(volSmile.AtmVolPoint)
        self.assertEqual(volSmile.Tenor, '1M')

        _atmVolPt = volSmile.AtmVolPoint
        self.assertEqual(_atmDnVolPt.AtmType, Atm.Dn)
        self.assertEqual(_atmDnVolPt.IsAtm, True)
        self.assertEqual(_atmDnVolPt.VolInPercentage, 8.23)
        self.assertEqual(_atmDnVolPt.VolInDecimal, 0.0823)

        _25dVolPts = volSmile.vol_smile_points_for_int_delta(25)
        self.assertEqual(len(_25dVolPts), 2)

        _25cVolPts = volSmile.vol_smile_points_for_decimal_delta(0.25)
        self.assertEqual(len(_25cVolPts), 2)

        _10dVolPts = volSmile.vol_smile_points_for_int_delta(10)
        self.assertEqual(len(_10dVolPts), 2)

        _10dVolPts = volSmile.vol_smile_points_for_decimal_delta(0.10)
        self.assertEqual(len(_10dVolPts), 2)

        _call_delta_volPts = volSmile.vol_smile_points_for_delta_orientation(DeltaOrientation.Call)
        _put_delta_volPts = volSmile.vol_smile_points_for_delta_orientation(DeltaOrientation.Put)
        self.assertEqual(len(_call_delta_volPts), 2)
        self.assertEqual(len(_put_delta_volPts), 2)

    def test_vol_smile_dict_codec(self):
        atmDnVolPt = VolatilitySmilePoint(8.23, Atm.Dn)
        sb25cVolPt = VolatilitySmilePoint(3.24, (25, DeltaOrientation.Call, DeltaType.SpotBp))
        sb10cVolPt = VolatilitySmilePoint(2.12, (10, DeltaOrientation.Call, DeltaType.SpotBp))
        sb25pVolPt = VolatilitySmilePoint(6.78, (25, DeltaOrientation.Put, DeltaType.SpotBp))
        sb10pVolPt = VolatilitySmilePoint(7.56, (10, DeltaOrientation.Put, DeltaType.SpotBp))
        volSmile = VolatilitySmile(tenor='1M',
                                   volSmilePoints=[sb10pVolPt, sb25pVolPt, atmDnVolPt, sb25cVolPt, sb10cVolPt])

        volSmile_to_dict = volSmile.to_dict()

        volSmile_from_dict = VolatilitySmile.from_dict(volSmile_to_dict)

        self.assertIsNotNone(volSmile_from_dict)
        self.assertEqual(volSmile_from_dict.Tenor, '1M')
        self.assertEqual(volSmile_from_dict.AtmVolPoint.VolInPercentage, 8.23)
        self.assertEqual(volSmile_from_dict.AtmVolPoint.AtmType, Atm.Dn)

        d25VolPts = volSmile.vol_smile_points_for_int_delta(25)
        d10VolPts = volSmile.vol_smile_points_for_int_delta(10)

        self.assertFalse(any(volPt.IsAtm for volPt in d25VolPts))
        self.assertFalse(any(volPt.IsAtm for volPt in d10VolPts))

        self.assertTrue(all(volPt.DeltaAsInt == 25 for volPt in d25VolPts))
        self.assertTrue(all(volPt.DeltaAsInt == 10 for volPt in d10VolPts))
        self.assertTrue(all(volPt.DeltaType is DeltaType.SpotBp for volPt in d25VolPts))
        self.assertTrue(all(volPt.DeltaType is DeltaType.SpotBp for volPt in d10VolPts))

    def test_vol_smile_json_codec(self):
        _atmDnVolPt = VolatilitySmilePoint(8.23, Atm.Dn)
        _25cVolPt = VolatilitySmilePoint(3.24, (25, DeltaOrientation.Call, DeltaType.SpotBp))
        _10cVolPt = VolatilitySmilePoint(2.12, (10, DeltaOrientation.Call, DeltaType.SpotBp))
        _25pVolPt = VolatilitySmilePoint(6.78, (25, DeltaOrientation.Put, DeltaType.SpotBp))
        _10pVolPt = VolatilitySmilePoint(7.56, (10, DeltaOrientation.Put, DeltaType.SpotBp))

        volSmile = VolatilitySmile(tenor='1M',
                                   volSmilePoints=[_10pVolPt, _25pVolPt, _atmDnVolPt, _25cVolPt, _10cVolPt])
        volSmile_dict = volSmile.to_dict()

        volSmile_json_obj = json.dumps(volSmile_dict)

        self.assertIsNotNone(volSmile_json_obj)

        volSmile_dict_decoded = json.loads(volSmile_json_obj)

        self.assertIsNotNone(volSmile_dict_decoded)

        self.assertEqual(volSmile_dict, volSmile_dict_decoded)

    def test_vol_surface_construction(self):
        vol_smile_1W = VolatilitySmile('1W', self._vol_smile_points_list_a)
        vol_smile_1M = VolatilitySmile('1M', self._vol_smile_points_list_a)
        vol_smile_3M = VolatilitySmile('3M', self._vol_smile_points_list_a)

        vol_smile_9M = VolatilitySmile('9M', self._vol_smile_points_list_b)
        vol_smile_1Y = VolatilitySmile('1Y', self._vol_smile_points_list_b)

        vol_smile_18M = VolatilitySmile('18M', self._vol_smile_points_list_c)
        vol_smile_2Y = VolatilitySmile('2Y', self._vol_smile_points_list_c)

        vol_surface = VolatilitySurface('EURUSD', dt.datetime.strptime('29 Mar 18', '%d %b %y'),
                                        [vol_smile_1W, vol_smile_1M, vol_smile_3M, vol_smile_9M, vol_smile_1Y,
                                         vol_smile_18M, vol_smile_2Y])

        self.assertIsNotNone(vol_surface)
        self.assertEqual(vol_surface.SecuritySymbol, 'eurusd'.upper())
        self.assertEqual(vol_surface.AsOfTimeStamp.year, 2018)
        self.assertEqual(vol_surface.AsOfTimeStamp.month, 3)
        self.assertEqual(vol_surface.AsOfTimeStamp.day, 29)
        self.assertGreater(len(vol_surface.VolSmiles), 0)
        self.assertEqual(len(vol_surface.VolSmiles), 7)

        vol_smile_10M = vol_surface.vol_smile_for_tenor('10M')
        vol_smile_1Y = vol_surface.vol_smile_for_tenor('1y')

        self.assertIsNone(vol_smile_10M)
        self.assertIsNotNone(vol_smile_1Y)

        _all25CallVolPts = vol_surface.vol_smile_points_for_delta(25, DeltaOrientation.Call)
        _all10CallVolPts = vol_surface.vol_smile_points_for_delta(10, DeltaOrientation.Call)

        _all25PutVolPts = vol_surface.vol_smile_points_for_delta(25, DeltaOrientation.Put)
        _all10PutVolPts = vol_surface.vol_smile_points_for_delta(10, DeltaOrientation.Put)

        atmVolPts = vol_surface.vol_smile_points_atm()
        atmVolPts_for_tenors = vol_surface.vol_smile_points_atm(tenors=['3M', '9M', '18M', '1Y'])
        atmVolPts_for_intersect_tenors = vol_surface.vol_smile_points_atm(tenors=['9M', '3M', '1Y', '18M', '10M', '5Y'])

        self.assertGreater(len(_all25CallVolPts), 0)
        self.assertGreater(len(_all10CallVolPts), 0)
        self.assertEqual(len(_all25CallVolPts), 7)
        self.assertEqual(len(_all10CallVolPts), 7)

        self.assertGreater(len(_all25PutVolPts), 0)
        self.assertGreater(len(_all10PutVolPts), 0)
        self.assertEqual(len(_all25PutVolPts), 7)
        self.assertEqual(len(_all10PutVolPts), 7)

        self.assertGreater(len(atmVolPts), 0)
        self.assertEqual(len(atmVolPts), 7)

        self.assertGreater(len(atmVolPts_for_tenors), 0)
        self.assertEqual(len(atmVolPts_for_tenors), 4)
        self.assertEqual(len(atmVolPts_for_intersect_tenors), 4)
        self.assertEqual(atmVolPts_for_intersect_tenors[0].VolInPercentage, 5.61)
        self.assertEqual(atmVolPts_for_intersect_tenors[2].VolInPercentage, 7.30)
        self.assertEqual(atmVolPts_for_intersect_tenors[3].VolInPercentage, 8.19)

        self.assertTrue(all([vol_smile.AtmVolPoint is not None for vol_smile in vol_surface.VolSmiles]))
        self.assertFalse(all([vol_smile.AtmVolPoint.AtmType is Atm.Dn for vol_smile in vol_surface.VolSmiles]))

    def test_try_add_vol_smiles(self):

        vol_smile_1W = VolatilitySmile('1W', self._vol_smile_points_list_a)
        vol_smile_1M = VolatilitySmile('1M', self._vol_smile_points_list_a)
        vol_smile_3M = VolatilitySmile('3M', self._vol_smile_points_list_a)
        vol_smile_9M = VolatilitySmile('9M', self._vol_smile_points_list_a)
        vol_smile_1Y = VolatilitySmile('1Y', self._vol_smile_points_list_a)

        vol_smile_18M = VolatilitySmile('18M', self._vol_smile_points_list_b)
        vol_smile_2Y = VolatilitySmile('2Y', self._vol_smile_points_list_b)

        vol_surface = VolatilitySurface('EURUSD', dt.datetime.strptime('29 Mar 18', '%d %b %y'),
                                        [vol_smile_1W, vol_smile_1M, vol_smile_3M, vol_smile_9M, vol_smile_1Y,
                                         vol_smile_18M, vol_smile_2Y])

        self.assertEqual(vol_surface.VolSmileTenors, ['1W', '1M', '3M', '9M', '1Y', '18M', '2Y'])

        vol_smile_4M = VolatilitySmile('4M', self._vol_smile_points_list_a)
        vol_smile_5M = VolatilitySmile('5M', self._vol_smile_points_list_a)

        add_result = vol_surface.try_add_vol_smiles([vol_smile_4M, vol_smile_5M])

        self.assertTrue(add_result)
        self.assertTrue('4M' in vol_surface.VolSmileTenors)
        self.assertTrue('5M' in vol_surface.VolSmileTenors)
        self.assertIsNotNone(vol_surface.vol_smile_for_tenor('4M'))
        self.assertIsNotNone(vol_surface.vol_smile_for_tenor('5M'))

        self.assertEqual(vol_surface.VolSmileTenors, ['1W', '1M', '3M', '4M', '5M', '9M', '1Y', '18M', '2Y'])

    def test_vol_surface_dict_codec(self):
        vol_smile_1W = VolatilitySmile('1W', self._vol_smile_points_list_a)
        vol_smile_1M = VolatilitySmile('1M', self._vol_smile_points_list_a)
        vol_smile_3M = VolatilitySmile('3M', self._vol_smile_points_list_a)

        vol_smile_9M = VolatilitySmile('9M', self._vol_smile_points_list_b)
        vol_smile_1Y = VolatilitySmile('1Y', self._vol_smile_points_list_b)

        vol_smile_18M = VolatilitySmile('18M', self._vol_smile_points_list_c)
        vol_smile_2Y = VolatilitySmile('2Y', self._vol_smile_points_list_c)

        vol_surface = VolatilitySurface('EURUSD', dt.datetime.strptime('29 Mar 18', '%d %b %y'),
                                        [vol_smile_1W, vol_smile_1M, vol_smile_3M, vol_smile_9M, vol_smile_1Y,
                                         vol_smile_18M, vol_smile_2Y])

        vol_surface_dict = vol_surface.to_dict()

        self.assertIsNotNone(vol_surface_dict)

        vol_surface_from_dict = VolatilitySurface.from_dict(vol_surface_dict)

        self.assertIsNotNone(vol_surface_from_dict)

    def test_vol_surface_json_codec(self):
        vol_smile_1W = VolatilitySmile('1W', self._vol_smile_points_list_a)
        vol_smile_1M = VolatilitySmile('1M', self._vol_smile_points_list_a)
        vol_smile_3M = VolatilitySmile('3M', self._vol_smile_points_list_a)

        vol_smile_9M = VolatilitySmile('9M', self._vol_smile_points_list_b)
        vol_smile_1Y = VolatilitySmile('1Y', self._vol_smile_points_list_b)

        vol_smile_18M = VolatilitySmile('18M', self._vol_smile_points_list_c)
        vol_smile_2Y = VolatilitySmile('2Y', self._vol_smile_points_list_c)

        vol_surface = VolatilitySurface('EURUSD', dt.datetime.strptime('29 Mar 18', '%d %b %y'),
                                        [vol_smile_1W, vol_smile_1M, vol_smile_3M, vol_smile_9M, vol_smile_1Y,
                                         vol_smile_18M, vol_smile_2Y])

        vol_surface_dict = vol_surface.to_dict()

        vol_surface_json_obj = json.dumps(vol_surface_dict)

        self.assertIsNotNone(vol_surface_json_obj)

        vol_surface_dict_decoded = json.loads(vol_surface_json_obj)

        self.assertIsNotNone(vol_surface_dict_decoded)

        self.assertEqual(vol_surface_dict, vol_surface_dict_decoded)

        self.assertIsNone(vol_surface.ReferenceSpot)
        self.assertIsNone(vol_surface.ReferenceDate)

        vol_surface.ReferenceSpot = 1.16075
        vol_surface.ReferenceDate = dt.datetime.strptime('29 Mar 18', '%d %b %y')

        self.assertIsNotNone(vol_surface.ReferenceSpot)
        self.assertIsNotNone(vol_surface.ReferenceDate)

        vol_surface_dict = vol_surface.to_dict()

        vol_surface_json_obj = json.dumps(vol_surface_dict)

        self.assertIsNotNone(vol_surface_json_obj)

        vol_surface_dict_decoded = json.loads(vol_surface_json_obj)

        self.assertIsNotNone(vol_surface_dict_decoded)

        self.assertEqual(vol_surface_dict, vol_surface_dict_decoded)

    def test_tenor_comparison(self):
        tenor_18M = Tenor('18M')
        tenor_3Y = Tenor('3Y')
        self.assertLess(tenor_18M, tenor_3Y)
        self.assertLessEqual(tenor_18M, tenor_3Y)
        self.assertNotEqual(tenor_18M, tenor_3Y)
        self.assertGreater(tenor_3Y, tenor_18M)
        self.assertGreaterEqual(tenor_3Y, tenor_18M)

        tenor_4W = Tenor('4W')
        tenor_1M = Tenor('1M')
        self.assertLess(tenor_4W, tenor_1M)
        self.assertLessEqual(tenor_4W, tenor_1M)
        self.assertNotEqual(tenor_4W, tenor_1M)
        self.assertGreater(tenor_1M, tenor_4W)
        self.assertGreaterEqual(tenor_1M, tenor_4W)

        tenor_5D = Tenor('5D')
        tenor_1W = Tenor('1W')
        self.assertLess(tenor_5D, tenor_1W)
        self.assertLessEqual(tenor_5D, tenor_1W)
        self.assertNotEqual(tenor_5D, tenor_1W)
        self.assertGreater(tenor_1W, tenor_5D)
        self.assertGreaterEqual(tenor_1W, tenor_5D)

        tenor_ON = Tenor('ON')
        tenor_1D = Tenor('1D')
        self.assertEqual(tenor_1D, tenor_ON)
