from typing import *

import datetime as dt
import json
import arrow


class TenorDates:

    def __init__(self, reference_date: dt.datetime, asof: dt.datetime, source: str, currency_pair: str,
                 tenor_maturity_dates: Dict[str, dt.datetime]) -> None:
        self._reference_date = reference_date
        self._asof = asof
        self._source = source
        self._currency_pair = currency_pair
        self._tenor_maturity_dates = tenor_maturity_dates

    @classmethod
    def from_json(cls, json_value: str) -> 'TenorDates':
        dict_value = json.loads(json_value)

        return cls.from_dict(dict_value)

    @classmethod
    def from_dict(cls, dict_value: dict) -> 'TenorDates':
        reference_date = arrow.get(dict_value['ReferenceDate']).datetime
        asof = arrow.get(dict_value['AsOf']).datetime
        source = dict_value['Source']
        currency_pair = dict_value['CurrencyPair']
        tenor_maturity_date_str_dict = dict_value['TenorMaturityDates']
        tenor_maturity_dates = {}
        for tenor in tenor_maturity_date_str_dict:
            maturity_date_str = tenor_maturity_date_str_dict[tenor]
            tenor_maturity_dates[tenor] = arrow.get(maturity_date_str).isoformat()

        return cls(reference_date, asof, source, currency_pair, tenor_maturity_dates)

    @property
    def ReferenceDate(self) -> dt.datetime:
        return self._reference_date

    @property
    def AsOf(self) -> dt.datetime:
        return self._as_of

    @property
    def Source(self) -> str:
        return self._source

    @property
    def CurrencyPair(self) -> str:
        return self._currency_pair

    @property
    def TenorMaturityDates(self) -> dict:
        return self._tenor_maturity_dates

    def GetMaturityDate(self, tenor: str) -> Union[dt.datetime, None]:
        if tenor in self._tenor_maturity_dates:
            return self._tenor_maturity_dates[tenor]
        else:
            return None
